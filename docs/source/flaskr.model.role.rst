flaskr.model.role module
========================

.. automodule:: flaskr.model.role
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
