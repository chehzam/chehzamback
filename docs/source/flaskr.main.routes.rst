flaskr.main.routes module
=========================

.. automodule:: flaskr.main.routes
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
