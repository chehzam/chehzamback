flaskr.gestion package
======================

.. automodule:: flaskr.gestion
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   flaskr.gestion.controllers
   flaskr.gestion.manager
