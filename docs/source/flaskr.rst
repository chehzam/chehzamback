flaskr package
==============

.. automodule:: flaskr
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 3

   flaskr.api
   flaskr.dataGeneration
   flaskr.dataModifiers
   flaskr.database
   flaskr.exception
   flaskr.gestion
   flaskr.main
   flaskr.model
