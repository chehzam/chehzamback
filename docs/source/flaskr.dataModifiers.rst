flaskr.dataModifiers package
============================

.. automodule:: flaskr.dataModifiers
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   flaskr.dataModifiers.audio_modifier
   flaskr.dataModifiers.downsampler
   flaskr.dataModifiers.frequency_filter
