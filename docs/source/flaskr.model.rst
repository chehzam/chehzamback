flaskr.model package
====================

.. automodule:: flaskr.model
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   flaskr.model.gateway

Submodules
----------

.. toctree::
   :maxdepth: 4

   flaskr.model.album
   flaskr.model.artist
   flaskr.model.audio_data
   flaskr.model.connection
   flaskr.model.cover
   flaskr.model.hash
   flaskr.model.music
   flaskr.model.musicHistory
   flaskr.model.role
   flaskr.model.token
   flaskr.model.user
