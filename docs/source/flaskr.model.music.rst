flaskr.model.music module
=========================

.. automodule:: flaskr.model.music
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
