.. CHEHZAM documentation master file, created by
   sphinx-quickstart on Fri May 26 10:46:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CHEHZAM's documentation!
===================================

Usage
=====

Installation
------------

To start the server, first install required modules using pip:

.. code-block:: console

   (.venv) $ pip install -r requirements.txt

Starting server
----------------

To start the application, first clone both *ChehZamBack* and *ChehZamFront* git repository.

Then, given the following repository structure, run the following code:

.. code-block:: console

   /
   ├── chahzamfront
   │   └── ...
   └── chehzamback
      └── ...

.. code-block:: console

   (.venv) $ cd ./chahzamback
   (.venv) $ docker build --pull --rm -f "dockerfile" -t chehzamback:latest "."
   (.venv) $ cd ../chahzamfront
   (.venv) $ docker build --pull --rm -f "dockerfile" -t chehzamfront:latest "."
   (.venv) $ cd ../chahzamback
   (.venv) $ docker compose -f "docker-compose.yml" up -d --build

Check that all process are working properly on Docker Desktop.

Connect to `localhost:8080 <http://localhost:8080/>`_.

API documentation
-----------------
.. raw:: html

   <a href="api.html">Here</a>

Contents
========

.. toctree::
   :maxdepth: 4

   modules
