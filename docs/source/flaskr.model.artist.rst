flaskr.model.artist module
==========================

.. automodule:: flaskr.model.artist
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
