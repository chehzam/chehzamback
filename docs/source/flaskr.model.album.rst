flaskr.model.album module
=========================

.. automodule:: flaskr.model.album
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
