flaskr.model.gateway.userGateway module
=======================================

.. automodule:: flaskr.model.gateway.userGateway
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
