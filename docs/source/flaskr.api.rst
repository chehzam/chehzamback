flaskr.api namespace
====================

.. py:module:: flaskr.api

Submodules
----------

.. toctree::
   :maxdepth: 4

   flaskr.api.apiAccessRigth
   flaskr.api.apiAuth
