flaskr.gestion.manager package
==============================

.. automodule:: flaskr.gestion.manager
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   flaskr.gestion.manager.analyse_manager
   flaskr.gestion.manager.manager
