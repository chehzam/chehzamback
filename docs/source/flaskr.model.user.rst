flaskr.model.user module
========================

.. automodule:: flaskr.model.user
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
