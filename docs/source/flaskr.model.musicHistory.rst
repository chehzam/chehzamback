flaskr.model.musicHistory module
================================

.. automodule:: flaskr.model.musicHistory
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
