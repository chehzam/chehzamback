flaskr.model.gateway.albumGateway module
========================================

.. automodule:: flaskr.model.gateway.albumGateway
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
