flaskr.main package
===================

.. automodule:: flaskr.main
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   flaskr.main.doc
   flaskr.main.routes
   flaskr.main.routes_test
