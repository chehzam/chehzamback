"""_Creation_tables_script _

  The script creates all the tables of the application
  by connecting to the PostgreSQL database with the module psycopg2. 
"""

import psycopg2

try:
    conn = psycopg2.connect(
          user = "postgres",
          password = "admin",  #To change
          host = "localhost",
          port = "5432",
          database = "CHEHZAM"
    )
    cur = conn.cursor()

    # --- DROP TABLE ROLE --- #
    sql = '''DROP TABLE IF EXISTS ROLE CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE TOKEN --- #
    sql = '''DROP TABLE IF EXISTS TOKEN CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE HASH --- #
    sql = '''DROP TABLE IF EXISTS HASH CASCADE; '''
    cur.execute(sql)
    conn.commit()

    # --- DROP TRIGGER trigger_history_limiter OF MUSIC_HISTORY --- #
    sql = '''DROP TRIGGER IF EXISTS trigger_history_limiter ON MUSIC_HISTORY CASCADE;'''
    cur.execute(sql)
    conn.commit()

    # --- DROP FUNCTION history_limiter --- #
    sql = '''DROP FUNCTION IF EXISTS history_limiter();'''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE MUSIC_HISTORY --- #
    sql = '''DROP TABLE IF EXISTS MUSIC_HISTORY CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE MUSIC_FEAT --- #
    sql = '''DROP TABLE IF EXISTS MUSIC_FEAT CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE MUSIC --- #
    sql = '''DROP TABLE IF EXISTS MUSIC CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE ALBUM --- #
    sql = '''DROP TABLE IF EXISTS ALBUM CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE ARTIST --- #
    sql = '''DROP TABLE IF EXISTS ARTIST CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE COVER --- #
    sql = '''DROP TABLE IF EXISTS COVER CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE CHEHZAM_USER_PASSWORD --- #
    sql = '''DROP TABLE IF EXISTS CHEHZAM_USER_PASSWORD CASCADE; '''
    cur.execute(sql)
    conn.commit()
    
    # --- DROP TABLE CHEHZAM_USER --- #
    sql = '''DROP TABLE IF EXISTS CHEHZAM_USER CASCADE; '''
    cur.execute(sql)
    conn.commit()
  
    #fermeture de la connexion à la base de données
    cur.close()
    conn.close()
    print("La connexion PostgreSQL est fermée")

# pylint: disable-next = broad-exception-caught
except (Exception, psycopg2.Error) as error :
    print ("Erreur lors de la connexion à PostgreSQL", error)