"""_Insertion_tables_script _

  The script insert into all the tables of the application a dataset
  by connecting to the PostgreSQL database with the module psycopg2. 
"""
import psycopg2


try:
    conn = psycopg2.connect(
        user="postgres",
        password="admin",  # To change
        host="localhost",
        port="5432",
        database="CHEHZAM"
    )
    cur = conn.cursor()

    # --- INSERT INTO TABLE ROLE --- #
    sql = "INSERT INTO ROLE (ID_ROLE, ROLE) VALUES (%s,%s)"
    value = (1, 'ADMIN')
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO ROLE (ID_ROLE, ROLE) VALUES (%s,%s)"
    value = (2, 'USER')
    cur.execute(sql, value)
    conn.commit()
    print("Enregistrement inséré avec succès dans la table ROLE.")

    # --- INSERT INTO TABLE CHEHZAM_USER --- #
    sql = "INSERT INTO CHEHZAM_USER (USER_NAME, FIRSTNAME, LASTNAME, ID_ROLE) VALUES (%s,%s,%s,%s)"
    value = ('Adrien.DENONFOUX@etu.uca.fr', 'Adrien', 'DENONFOUX', 1)
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER (USER_NAME, FIRSTNAME, LASTNAME, ID_ROLE) VALUES (%s,%s,%s,%s)"
    value = ('Yannis.ROCHE@etu.uca.fr', 'Yannis', 'ROCHE', 1)
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER (USER_NAME, FIRSTNAME, LASTNAME, ID_ROLE) VALUES (%s,%s,%s,%s)"
    value = ('Yoann.GOURVES@etu.uca.fr', 'Yoann', 'GOURVES', 1)
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER (USER_NAME, FIRSTNAME, LASTNAME, ID_ROLE) VALUES (%s,%s,%s,%s)"
    value = ('Baptiste.MARTEL@etu.uca.fr', 'Baptiste', 'MARTEL', 1)
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER (USER_NAME, FIRSTNAME, LASTNAME, ID_ROLE) VALUES (%s,%s,%s,%s)"
    value = ('Thomas.PITO@etu.uca.fr', 'Thomas', 'PITO', 2)
    cur.execute(sql, value)
    conn.commit()
    print("Enregistrement inséré avec succès dans la table ROLE.")

    # --- INSERT INTO TABLE CHEHZAM_USER_PASSWORD --- #
    # MDP : 1234
    sql = "INSERT INTO CHEHZAM_USER_PASSWORD (ID_USER, PASSWORD) VALUES (%s,%s)"
    value = (1, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER_PASSWORD (ID_USER, PASSWORD) VALUES (%s,%s)"
    value = (2, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER_PASSWORD (ID_USER, PASSWORD) VALUES (%s,%s)"
    value = (3, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER_PASSWORD (ID_USER, PASSWORD) VALUES (%s,%s)"
    value = (4, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
    cur.execute(sql, value)
    conn.commit()
    sql = "INSERT INTO CHEHZAM_USER_PASSWORD (ID_USER, PASSWORD) VALUES (%s,%s)"
    value = (5, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
    cur.execute(sql, value)
    conn.commit()
    print("Enregistrement inséré avec succès dans la table CHEHZAM_USER_PASSWORD.")

    # fermeture de la connexion à la base de données
    cur.close()
    conn.close()
    print("La connexion PostgreSQL est fermée")

# pylint: disable-next = broad-exception-caught
except (Exception, psycopg2.Error) as error:
    print("Erreur lors de la connexion à PostgreSQL", error)
