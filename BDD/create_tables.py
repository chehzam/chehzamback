"""_Creation_tables_script _

  The script creates all the tables of the application
  by connecting to the PostgreSQL database with the module psycopg2. 
"""

import psycopg2

try:
    conn = psycopg2.connect(
          user = "postgres",
          password = "admin",  #To change
          host = "localhost",
          port = "5432",
          database = "CHEHZAM"
    )
    cur = conn.cursor()
    
    # --- CREATE TABLE ROLE --- #
    sql = '''CREATE TABLE ROLE(
        ID_ROLE INT NOT NULL,
        ROLE VARCHAR(5) NOT NULL,
        PRIMARY KEY (ID_ROLE)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table ROLE créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE CHEHZAM_USER --- #
    sql = '''CREATE TABLE CHEHZAM_USER(
        ID_USER SERIAL NOT NULL,
        USER_NAME VARCHAR(50) NOT NULL UNIQUE,
        FIRSTNAME VARCHAR(32) NOT NULL,
        LASTNAME VARCHAR(32) NOT NULL,
        ID_ROLE INT NOT NULL,
        PRIMARY KEY (ID_USER),
        FOREIGN KEY (ID_ROLE) REFERENCES ROLE(ID_ROLE)
      ); '''
    
    
    cur.execute(sql)
    conn.commit()
    print("Table CHEHZAM_USER créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE CHEHZAM_USER_PASSWORD --- #
    sql = '''CREATE TABLE CHEHZAM_USER_PASSWORD(
        ID_USER INT NOT NULL,
        PASSWORD VARCHAR(512) NOT NULL,
        PRIMARY KEY (ID_USER),
        FOREIGN KEY (ID_USER) REFERENCES CHEHZAM_USER(ID_USER)
      ); '''
    
    
    cur.execute(sql)
    conn.commit()
    print("Table CHEHZAM_USER_PASSWORD créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE COVER --- #
    sql = '''CREATE TABLE COVER(
        ID_COVER SERIAL NOT NULL,
        IMAGE BYTEA NOT NULL,
        PRIMARY KEY (ID_COVER)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table COVER créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE ARTIST --- #
    sql = '''CREATE TABLE ARTIST(
        ID_ARTIST SERIAL NOT NULL,
        NAME VARCHAR(32) NOT NULL,
        PRIMARY KEY (ID_ARTIST)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table ARTIST créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE ALBUM --- #
    sql = '''CREATE TABLE ALBUM(
        ID_ALBUM SERIAL NOT NULL,
        TITLE VARCHAR(32) NOT NULL,
        ID_COVER INT NOT NULL,
        PRIMARY KEY (ID_ALBUM),
        FOREIGN KEY (ID_COVER) REFERENCES COVER(ID_COVER)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table ALBUM créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE MUSIC --- #
    sql = '''CREATE TABLE MUSIC(
        ID_MUSIC SERIAL NOT NULL,
        ID_ARTIST INT NOT NULL,
        TITLE VARCHAR(32) NOT NULL,
        ID_ALBUM INT NOT NULL,
        PRIMARY KEY (ID_MUSIC),
        FOREIGN KEY (ID_ALBUM) REFERENCES ALBUM(ID_ALBUM),
        FOREIGN KEY (ID_ARTIST) REFERENCES ARTIST(ID_ARTIST)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table MUSIC créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE MUSIC_ARTIST --- #
    sql = '''CREATE TABLE MUSIC_FEAT(
        ID_MUSIC INT NOT NULL,
        ID_ARTIST INT NOT NULL,
        PRIMARY KEY (ID_MUSIC,ID_ARTIST),
        FOREIGN KEY (ID_MUSIC) REFERENCES MUSIC(ID_MUSIC),
        FOREIGN KEY (ID_ARTIST) REFERENCES ARTIST(ID_ARTIST)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table MUSIC_FEAT créée avec succès dans PostgreSQL")

    # --- CREATE TABLE MUSIC_HISTORY --- #
    sql = '''CREATE TABLE MUSIC_HISTORY(
        ID_MUSIC_HISTORY SERIAL NOT NULL,
        ID_USER INT NOT NULL,
        ID_MUSIC INT NOT NULL,
        HISTORY_DATE TIMESTAMP NOT NULL,
        PRIMARY KEY (ID_MUSIC_HISTORY),
        FOREIGN KEY (ID_USER) REFERENCES CHEHZAM_USER(ID_USER),
        FOREIGN KEY (ID_MUSIC) REFERENCES MUSIC(ID_MUSIC)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table MUSIC_HISTORY créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE TOKEN --- #
    sql = '''CREATE TABLE TOKEN(
        TOKEN VARCHAR(256) NOT NULL,
        EXPIRATION_DATE DATE NOT NULL,
        ID_USER INT NOT NULL,
        PRIMARY KEY (TOKEN),
        FOREIGN KEY (ID_USER) REFERENCES CHEHZAM_USER(ID_USER)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table TOKEN créée avec succès dans PostgreSQL")
    
    # --- CREATE TABLE HASH --- #
    sql = '''CREATE TABLE HASH(
        FINGERPRINT BIGINT NOT NULL,
        COUPLE BIGINT NOT NULL,
        PRIMARY KEY (FINGERPRINT, COUPLE)
      ); '''
    
    cur.execute(sql)
    conn.commit()
    print("Table HASH créée avec succès dans PostgreSQL")

    history_trigger = '''
    CREATE FUNCTION history_limiter() RETURNS trigger
        LANGUAGE plpgsql
        AS $$
        DECLARE
            row_nb INTEGER := (SELECT count(*) from MUSIC_HISTORY M WHERE M.ID_USER = NEW.ID_USER);
        BEGIN
            while row_nb > 10 loop
                DELETE FROM MUSIC_HISTORY M
                WHERE M.HISTORY_DATE = (
                    SELECT MIN(M.HISTORY_DATE)
                    FROM MUSIC_HISTORY M
                    WHERE M.ID_USER = NEW.ID_USER
                )
                AND M.ID_USER = NEW.ID_USER;
                row_nb := (SELECT count(*) from MUSIC_HISTORY M WHERE M.ID_USER = NEW.ID_USER);
            end loop;
            RETURN NULL;
        END;
        $$;

    CREATE TRIGGER trigger_history_limiter
    AFTER INSERT ON MUSIC_HISTORY
    FOR EACH ROW
    EXECUTE PROCEDURE history_limiter();
    '''

    cur.execute(history_trigger)
    conn.commit()
    print("Triger history_trigger publié")
  
    #fermeture de la connexion à la base de données
    cur.close()
    conn.close()
    print("La connexion PostgreSQL est fermée")

# pylint: disable-next = broad-exception-caught
except (Exception, psycopg2.Error) as error :
    print ("Erreur lors de la connexion à PostgreSQL", error)
