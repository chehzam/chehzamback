"""
This class defines access rights for the API routes based on routes, HTTP methods, and user access levels.

Attributes:
- accessRigth (list): List of dictionaries containing access rights information for different routes and methods.

Methods:
- __init__: Initializes an instance of the ApiAccessRigth class.
- checkAccessRigth: Checks if a given route, method, and access level have access rights.

"""

import re


class ApiAccessRigth:
    #define the access rigth for the API
    accessRigth=[]
    #access rigth for user
    accessRigth.append({'route':'/user/x','methods':['GET','PUT','DELETE'],'access':['admin','user']})
    accessRigth.append({'route':'/user','methods':['GET','DELETE'],'access':['admin']})
    #access rigth for cover
    accessRigth.append({'route':'/cover/x','methods':['GET'],'access':['admin','user']})
    accessRigth.append({'route':'/cover/x','methods':['PUT','DELETE'],'access':['admin']})
    accessRigth.append({'route':'/cover','methods':['GET','DELETE'],'access':['admin']})
    #access rigth for author
    accessRigth.append({'route':'/author/x','methods':['GET'],'access':['admin','user']})
    accessRigth.append({'route':'/author/x','methods':['PUT','DELETE'],'access':['admin']})
    accessRigth.append({'route':'/author','methods':['GET','POST','PUT','DELETE'],'access':['admin']})
    #access rigth for album
    accessRigth.append({'route':'/album/x','methods':['GET'],'access':['admin','user']})
    accessRigth.append({'route':'/album/x','methods':['PUT','DELETE'],'access':['admin']})
    accessRigth.append({'route':'/album','methods':['GET','POST','PUT','DELETE'],'access':['admin']})
    #access rigth for music
    accessRigth.append({'route':'/music','methods':['POST'],'access':['admin','user']})
    accessRigth.append({'route':'/music/x','methods':['GET'],'access':['admin','user']})
    accessRigth.append({'route':'/music','methods':['GET','DELETE'],'access':['admin']})
    accessRigth.append({'route':'/music/x','methods':['GET','PUT','DELETE'],'access':['admin']})
    #access rigth for history
    accessRigth.append({'route':'/history/x','methods':['GET','DELETE','POST'],'access':['admin','user']})
    #acces rigth for addMusic
    accessRigth.append({'route':'/addMusic','methods':['POST'],'access':['admin']})
    #access rigth for connection
    accessRigth.append({'route':'/connexion','methods':['DELETE'],'access':['admin','user']})
    accessRigth.append({'route':'/connexion/login','methods':['POST'],'access':['admin','user']})
    accessRigth.append({'route':'/connexion/signin','methods':['POST'],'access':['admin','user']})

    def checkAccessRigth(self,route:str,method:str,access:str):
        """
        Checks if a given route, method, and access level have access rights.

        Parameters:
        - route (str): The route to check access rights for.
        - method (str): The HTTP method to check access rights for.
        - access (str): The access level to check access rights for.

        Returns:
        - bool: True if the given route, method, and access level have access rights, False otherwise.

        """

        #replace the /<something> by /x
        route = re.sub('/<.*>', '/x', route)
        return any(rigth['route'] == route and method in rigth['methods'] and access.lower() in rigth['access'] for rigth in self.accessRigth)
