"""
This class provides authentication and authorization functionalities for API access.

Attributes:
- tokenLength (int): The length of generated tokens.

Methods:
- __init__: Initializes an instance of the ApiAuth class.
- passwordSignin: Creates a new user with a password and generates a token for signing in.
- passwordLogin: Verifies the username and password of a user and generates a token for login.
- tokenLogin: Verifies the validity of a token for login.
- tokenLogout: Logs out a user by deleting their token.
- tokenRefresh: Refreshes the expiry time of a token.
- tokenVerify: Verifies the validity of a token.
- tokenRevoke: Revokes a token by logging out the user.
- generateToken: Generates a token for a given username.
- getRole: Retrieves the role of a user based on their token.
- getUser: Retrieves the user object associated with a token.
"""

from __future__ import annotations
from datetime import datetime, timedelta, timezone
import random
import string

from flask import request
from flaskr.model.user import User
from model.gateway.tokenGateway import TokenGateway
from model.gateway.userGateway import UserGateway
from model.role import Role
from model.token import Token


class ApiAuth:
    tokenLength = 256
    def __init__(self):
        """
        Initializes an instance of the ApiAuth class.
        """

        self.tokenGateway=TokenGateway()
        self.userGateway=UserGateway()

    def passwordSignin(self, username:str, firstname:str, lastname:str, password:str)->Token:
        """
        Creates a new user with a password and generates a token for signing in.

        Parameters:
        - username (str): The username of the user.
        - firstname (str): The firstname of the user.
        - lastname (str): The lastname of the user.
        - password (str): The password of the user.

        Returns:
        - Token: The generated token.

        """
        #create user
        self.userGateway.insert(username,firstname,lastname, password,Role.USER)  
        token = self.generateToken(username)
        self.tokenGateway.insert(token)
        return token

    def passwordLogin(self, username:str, password:str)->Token:
        """
        Verifies the username and password of a user and generates a token for login.

        Parameters:
        - username (str): The username of the user.
        - password (str): The password of the user.

        Returns:
        - Token: The generated token if the user exists and the password is correct, otherwise an undefined token.

        """
        #verify if user exists and if password is correct
        #if yes, generate token with associated rigths
        if self.userGateway.exist(username, password):
            token = self.generateToken(username)
            self.tokenGateway.insert(token)
            return token
        #if no, return undefined token
        return Token("", datetime.now(timezone.utc))    
    
    def tokenLogin(self, token:str)->bool:
        """
        Verifies the validity of a token for login.

        Parameters:
        - token (str): The token to verify.

        Returns:
        - bool: True if the token is valid, False otherwise.

        """
        #verify if token is valid
        #if yes, return user rigths
        if self.tokenGateway.get(token):
            return True
        #if no, return false
        return False
    
    def tokenLogout(self, token:str)->None:
        """
        Logs out a user by deleting their token.

        Parameters:
        - token (str): The token to log out.

        """
        #verify if token is valid
        #if yes, delete token
        tokenObj = self.tokenGateway.get(token)
        if tokenObj:
            self.tokenGateway.delete(tokenObj)
        
    
    def tokenRefresh(self, token:str)->None:
        """
        Refreshes the expiry time of a token.

        Parameters:
        - token (str): The token to refresh.

        """
        #verify if token is valid
        #if yes, refresh token
        tokenObj = self.tokenGateway.get(token)
        if tokenObj:
            tokenObj.expire_time = datetime.now(timezone.utc) + timedelta(days=1)
            self.tokenGateway.update(tokenObj)
    
    def tokenVerify(self, token:str)->bool:
        #verify if token is valid
        #if yes, return true
        tokenObj = self.tokenGateway.get(token)
        if tokenObj:
            return True
        return False
    
    def tokenRevoke(self, token:str)->None:
        #verify if token is valid
        #if yes, revoke token
        self.tokenLogout(token)
    
    def generateToken(self, username:str)->Token:
        randomString = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(self.tokenLength))
        #if user asked to be remenbered, set expire time to 1 year
        #else, set expire time to 1 day
        if request.form.get("remember"):
            expire_time = datetime.now(timezone.utc) + timedelta(days=365)
        else:
            expire_time = datetime.now(timezone.utc) + timedelta(days=1)
        user=self.userGateway.getByUsername(username)
        if user:
            return Token(randomString, expire_time,user)
        return Token('', datetime.now(timezone.utc))
    
    def getRole(self, token:str)->Role:
        #verify if token is valid
        if self.tokenVerify(token):
            #if yes, return user rigths
            user=self.tokenGateway.get(token).User
            if user is not None:
                return user.role if user.role is not None else Role.NONE
        return Role.NONE
    
    def getUser(self, token:str)->User | None:
        #verify if token is valid
        if self.tokenVerify(token):
            #if yes, return user rigths
            return self.tokenGateway.get(token).User
        return None