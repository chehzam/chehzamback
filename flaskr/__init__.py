"""
This script creates a Flask application with routes and blueprints for various API endpoints. It also initializes necessary configurations and dependencies.

Dependencies:
- os
- flask_cors.CORS
- scipy.io.wavfile
- flask.Flask
- flask.Blueprint
- main.routesBp
- main.routes_testBp
- main.docBp
- main.auto
- dataGeneration.DataGenerator
- model.audio_data.AudioData
- dataModifiers.frequency_filter.FrequencyFilter
- dataModifiers.audio_modifier.AudioModifier

"""
import os
from flask_cors import CORS
import scipy.io.wavfile as wavfile
from flask import Flask, Blueprint, g

from main import routesBp as api_bp
from main import routes_testBp as test_bp
from main import docBp as doc_bp
from main import auto

from dataGeneration.data_generator import DataGenerator
from model.audio_data import AudioData
from dataModifiers.frequency_filter import FrequencyFilter
from dataModifiers.audio_modifier import AudioModifier


def create_app(test_config=None):
    """
    Creates a Flask application with routes and blueprints for various API endpoints. It also initializes necessary configurations and dependencies.

    Parameters:
    - test_config (dict): Configuration dictionary for testing (default: None)

    Returns:
    - app (Flask): Flask application object

    """

    cors=CORS()

    app = Flask(__name__, instance_relative_config=True)
    app.config['CORS_HEADERS'] = 'Content-Type'
    cors.init_app(app=app, supports_credentials=True)

    auto.init_app(app)

    app.register_blueprint(api_bp,CORS=cors)
    app.register_blueprint(doc_bp,CORS=cors)


    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
        return response


    @app.route('/')
    def index():
        """
        Renders the index page with links to various API endpoints.

        Returns:
        - str: HTML content with links to API endpoints

        """
        
        return '\
            <a href="/api/v1/spectrogram"> /api/v1/spectrogram </a><br/>\
            <a href="/api/v1/peak_extractor"> /api/v1/peak_extractor </a><br/>\
            <a href="/api/v1/hash_generator"> /api/v1/hash_generator </a><br/>\
            <a href="/api/v1/hash_match"> /api/v1/hash_match </a><br/>\
            <a href="/api/v1/peak_spectrum"> /api/v1/peak_spectrum </a><br/>\
            <a href="/api/v1/import_music"> /api/v1/import_music </a><br/>\
        '

    return app
