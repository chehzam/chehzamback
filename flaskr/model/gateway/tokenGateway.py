from datetime import datetime
from database.queryExecutor import QueryExecutor
from model.token import Token
from model.user import User
from model.role import Role
from model.gateway.userGateway import UserGateway



class TokenGateway:
    
    def get(self, token:str) -> Token:
        queryExecutor = QueryExecutor()
        sql = "SELECT token, expiration_date, id_user FROM token WHERE token = %s"
        queryExecutor.execute(sql, (token,))
        res = queryExecutor.fetchone()
        if res is not None:
            token, expiration_date, id_user = res
            userGat = UserGateway()
            util = userGat.get_id(id_user)
            return Token(token, expiration_date, User(util.id_user, util.user_name, util.firstname, util.lastname, util.role))
        return Token('',datetime.today(),User(0,'','','',Role.USER))


    def insert(self, token:Token)->None:
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO token (token, expiration_date, id_user) VALUES (%s,%s,%s)"
        user = token.User
        if user:
            value = (token.token, token.expire_time, user.id_user)
            queryExecutor.execute(sql, value)

    def update(self, token:Token)->None:
        queryExecutor = QueryExecutor()
        sql = "UPDATE token SET expiration_date = %s WHERE token = %s"
        value = (token.expire_time, token.token)
        queryExecutor.execute(sql, value)
    
    def delete(self, token:Token)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM token WHERE token = %s"
        queryExecutor.execute(sql, (token.token, ))

    
