from database.queryExecutor import QueryExecutor
from model.album import Album
from model.artist import Artist
from model.gateway.coverGateway import CoverGateway
from exception.noMatchExeption import NoMatchException

class AlbumGateway:
    
    def get(self)->list[Album]:
        queryExecutor = QueryExecutor()
        sql = '''SELECT id_album, title, id_cover FROM album;'''
        queryExecutor.execute(sql)
        res = queryExecutor.fetchall() 
        albums = []
        for id_album, title, id_cover in res:
            cover = CoverGateway().get_id(id_cover)
            albums.append(Album(id_album, title, cover))
        return albums
        
    def get_id(self, id_album:int)->Album:
        queryExecutor = QueryExecutor()
        sql = '''SELECT id_album, title, id_cover FROM album WHERE id_album = %s;'''
        queryExecutor.execute(sql, (id_album,))
        res = queryExecutor.fetchone()
        if res is not None:
            id_album, title, id_cover = res
            cover = CoverGateway().get_id(id_cover)
            return Album(id_album, title, cover)
        raise NoMatchException()
        
    def insert(self, album:Album)->Album:
        if album.cover is None:
            raise RuntimeError()
        queryExecutor = QueryExecutor()
        sql = '''INSERT INTO album (title, id_cover) VALUES (%s,%s) RETURNING id_album;'''
        value = (album.title, album.cover.id_cover)
        queryExecutor.cur.execute(sql, value)
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        if res is None:
            raise NoMatchException()
        album.id_album = res[0]
        return album
        
    def update(self, album:Album)->None:
        if album.cover is None:
            raise RuntimeError()
        queryExecutor = QueryExecutor()
        sql = '''UPDATE album SET title = %s, id_cover = %s , WHERE id_album = %s;'''
        value = (album.title, album.cover.id_cover, album.id_album)
        queryExecutor.execute(sql, value)
        
    def delete_id(self, id_album:int)->None:
        queryExecutor = QueryExecutor()
        sql = '''DELETE FROM album WHERE id_album = %s;'''
        queryExecutor.execute(sql, (id_album,))
        
    def delete(self)->None:
        queryExecutor = QueryExecutor()
        sql = '''DELETE FROM album;'''
        queryExecutor.execute(sql)
        
    def get_by_titre(self, album: Album, artist: Artist)->Album:
        queryExecutor = QueryExecutor()
        sql = '''SELECT alb.id_album, alb.id_cover
            FROM album AS alb INNER JOIN music AS msc ON msc.id_album = alb.id_album
            WHERE alb.title = %s AND msc.id_artist = %s'''
        values = (album.title, artist.id_artist)
        queryExecutor.execute(sql, values)
        res = queryExecutor.fetchone()
        if res is not None:
            id_album, id_cover = res
            cover = CoverGateway().get_id(id_cover)
            album.id_album = id_album
            album.cover = cover
        return album