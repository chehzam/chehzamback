from __future__ import annotations

from model.music import Music
from model.artist import Artist
from model.gateway.albumGateway import AlbumGateway
from model.gateway.artistGateway import ArtistGateway
from database.queryExecutor import QueryExecutor
from exception.noMatchExeption import NoMatchException

class MusicGateway:
    
    def get(self)->list[Music]:
        queryExecutor = QueryExecutor()
        sql = '''SELECT id_music, title, id_album, id_artist
            FROM music
            '''
        queryExecutor.execute(sql)
        res = queryExecutor.fetchall()
        musics = []
        for id_music, title, id_album, id_artist in res:
            feats = self.get_feats(id_music)
            album = AlbumGateway().get_id(id_album)
            artist = ArtistGateway().get_id(id_artist)
            musics.append(Music(id_music, title, artist, feats, album))

        return musics
        
    def get_feats(self, id_music:int)->list[Artist]:
        queryExecutor = QueryExecutor()
        sql = '''SELECT art.id_artist, art.name
            FROM artist art inner join music_feat msc_ft
            ON art.id_artist = msc_ft.id_artist
            WHERE msc_ft.id_music = %s
            '''
        queryExecutor.execute(sql, (id_music,))
        res = queryExecutor.fetchall()
        feats = []
        for id_artist, name in res:
            feats.append(Artist(id_artist, name))
        return feats

    def get_id(self, id_music:int)->Music:
        queryExecutor = QueryExecutor()
        sql = '''SELECT msc.id_music, msc.title, msc.id_album, msc.id_artist
            FROM music msc
            WHERE msc.id_music = %s
            '''
        value = (id_music,)
        queryExecutor.execute(sql, value)
        res = queryExecutor.fetchone()
        if res is not None:
            id_music, title, id_album, id_artist = res
            feats = self.get_feats(id_music)
            album = AlbumGateway().get_id(id_album)
            artist = ArtistGateway().get_id(id_artist)
            return Music(id_music, title, artist, feats, album)
        raise NoMatchException()
    
    def get_artist_by__id(self, id_music:int) -> list[Artist]:
        queryExecutor = QueryExecutor()
        sql = '''SELECT art.id_artist, art.name
            FROM music msc INNER JOIN artist art
            ON msc.id_artist = art.id_artist
            WHERE msc.id_music = %s
            '''
        value = (id_music,)
        queryExecutor.execute(sql, value)
        res = queryExecutor.fetchall() 
        artists = []
        for id_artist, name in res:
            artists.append(Artist(id_artist, name))
        return artists
        
    #Insert Music with Artist, Cover and Album
    def insert(self, music:Music)->Music:
        if music.artist is None or music.album is None or music.featuring is None:
            raise RuntimeError()
        queryExecutor = QueryExecutor()
        
        sql = "INSERT INTO music (title, id_artist, id_album) VALUES (%s,%s,%s) RETURNING id_music;"
        value = (music.title, music.artist.id_artist, music.album.id_album)

        queryExecutor.cur.execute(sql, value)
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        id_music = res[0] if res is not None else 0
        
        for artist in music.featuring:
            sql = "INSERT INTO music_feat (id_music, id_artist) VALUES (%s,%s)"
            value = (id_music, artist.id_artist)
            queryExecutor.execute(sql, value)

        return Music(id_music, music.title, music.artist, music.featuring, music.album)
    
    def update(self, music:Music)->None:
        if music.album is None:
            raise RuntimeError()
        queryExecutor = QueryExecutor()
        sql = "UPDATE music SET title = %s, id_album = %s , WHERE id_music = %s"
        value = (music.title, music.album.id_album, music.id_music)
        queryExecutor.execute(sql, value)
        
    def delete_id(self, id_music:int)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM music WHERE id_music = %s"
        queryExecutor.execute(sql, (id_music, ))
        
    def delete(self)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM music"
        queryExecutor.execute(sql)
        
    def exist(self, id_music:int)->bool:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_music FROM music WHERE id_music = %s"
        queryExecutor.execute(sql, (id_music,))
        res = queryExecutor.fetchone() 
        if res is None:
            return False
        return True
    
    def get_by_title(self, music: Music)->Music:
        if music.album is None:
            raise RuntimeError()
        queryExecutor = QueryExecutor()
        sql = '''SELECT id_music
            FROM music
            WHERE title = %s AND id_album = %s
        '''
        values = (music.title, music.album.id_album)
        queryExecutor.execute(sql, values)
        res = queryExecutor.fetchone()
        if res is not None:
            #print("Id_music = ", res[0], "\n")
            music.id_music = res[0]
        return music
        