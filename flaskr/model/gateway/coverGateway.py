from database.queryExecutor import QueryExecutor
from model.cover import Cover
from exception.noMatchExeption import NoMatchException

class CoverGateway:

    def get(self)->list[Cover]:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_cover, image FROM cover"
        queryExecutor.execute(sql)
        res = queryExecutor.fetchall() 
        covers = []
        for id_cover, image in res:
            covers.append(Cover(id_cover, bytes(image)))
        return covers
        
    def get_id(self, id_cover:int)->Cover:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_cover, image FROM cover WHERE Id_cover = %s"
        values = (id_cover,)
        queryExecutor.execute(sql, values)
        res = queryExecutor.fetchone()
        if res is not None:
            id_cover, image = res
            return Cover(id_cover, bytes(image))
        raise NoMatchException()
        
    def insert(self, cover:Cover)->Cover:
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO cover (image) VALUES (%s) RETURNING id_cover;"
        value = (cover.image,)
        queryExecutor.cur.execute(sql, value)
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        if res is not None:
            return Cover(res[0], bytes(cover.image))
        raise NoMatchException()
        
                
    def update(self, cover:Cover)->None:
        queryExecutor = QueryExecutor()
        sql = "UPDATE cover SET image = %s WHERE id_user = %s"
        value = (str(cover.image), cover.id_cover)
        queryExecutor.execute(sql, value)
        
    def delete_id(self, id_cover:int)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM cover WHERE id_cover = %s"
        queryExecutor.execute(sql, (id_cover, ))
        
    def delete(self)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM cover"
        queryExecutor.execute(sql)
        