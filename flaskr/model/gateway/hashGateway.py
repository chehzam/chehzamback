from __future__ import annotations
from database.queryExecutor import QueryExecutor
from psycopg2.extras import execute_values

from model.hash import Hash

class HashGateway:
    def __init__(self) -> None:
        self.fMask=281470681743360
        self.tMask=4294967295
        
    def get(self, fingerprint_tuple: tuple)->list[Hash]:
        queryExecutor = QueryExecutor()

        sql = "SELECT fingerprint, couple FROM hash WHERE fingerprint IN %s"
        values = (fingerprint_tuple,)
        queryExecutor.execute(sql, values)
        res = queryExecutor.fetchall() 
        hashs = []
        for fingerprint, couple in res:
            hashs.append(Hash(fingerprint, couple))
        return hashs
    
    def insertHashs(self, data:list)->None:
        queryExecutor = QueryExecutor()

        ratio_Tuple=tuple(map(self.computeRatio,data))
        
        data=list((data[i][0],data[i][1],ratio_Tuple[i]) for i in range(len(data)))

        sql = "INSERT INTO public.hash VALUES %s"
        execute_values(queryExecutor.cur, sql, tuple(data))
        queryExecutor.conn.commit()
        
    def insert(self, h: Hash)->Hash:
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO hash (fingerprint, couple) VALUES (%s,%s)"
        values = (h.fingerprint, h.couple,)
        queryExecutor.cur.execute(sql, values)
        queryExecutor.conn.commit()
        return h
        
    def delete(self)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM hash"
        queryExecutor.execute(sql)
        
    
    def computeRatio(self, fingerprint:tuple)->float:
        return ((fingerprint[0]>>48)-((fingerprint[0]&self.fMask)>>32))/(fingerprint[0]&self.tMask)