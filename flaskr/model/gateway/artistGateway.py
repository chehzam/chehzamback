from database.queryExecutor import QueryExecutor
from model.artist import Artist
from exception.noMatchExeption import NoMatchException

class ArtistGateway:
    
    def get(self)->list[Artist]:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_artist, name FROM artist"
        queryExecutor.execute(sql)
        res = queryExecutor.fetchall() 
        artists = []
        for id_artist, name in res:
            artists.append(Artist(id_artist, name))
        return artists
        
    def get_id(self, id_artist:int)->Artist:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_artist, name FROM artist WHERE id_artist = %s"
        values = (id_artist,)
        queryExecutor.execute(sql, values)
        res = queryExecutor.fetchone()
        if res is not None:
            id_artist, name = res
            return Artist(id_artist, name)
        raise NoMatchException()
    
    def get_by_name(self, artist: Artist)->Artist:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_artist FROM artist WHERE name = %s"
        value = (artist.name,)
        queryExecutor.execute(sql, value)
        res = queryExecutor.fetchone()
        if res is not None:
            artist.id_artist = res[0]
        return artist
    
    def exist(self, id_artist:int)->bool:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_artist FROM artist WHERE id_artist = %s"
        queryExecutor.execute(sql, (id_artist,))
        res = queryExecutor.fetchone() 
        if res is None:
            return False
        return True
        
    def insert(self, artist:Artist)->Artist:
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO artist (name) VALUES (%s) RETURNING id_artist;"
        queryExecutor.cur.execute(sql, (artist.name,))
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        if res is not None:
            return Artist(res[0], artist.name)
        raise NoMatchException()
        
    def update(self, artist:Artist)->None:
        queryExecutor = QueryExecutor()
        sql = "UPDATE artist SET name = %s WHERE id_artist = %s"
        value = (artist.name, artist.id_artist)
        queryExecutor.execute(sql, value)
        
    def delete_id(self, id_artist:int)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM artist WHERE id_artist = %s"
        queryExecutor.execute(sql, (id_artist, ))
        
    def delete(self)->None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM artist"
        queryExecutor.execute(sql)
        