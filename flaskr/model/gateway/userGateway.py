from __future__ import annotations
from database.queryExecutor import QueryExecutor

from model.user import User
from model.role import Role
from model.music import Music
from model.musicHistory import MusicHistory
from exception.noMatchExeption import NoMatchException
from model.gateway.musicGateway import MusicGateway


class UserGateway:

    def get(self) -> list[User]:
        queryExecutor = QueryExecutor()
        sql = "SELECT usr.id_user, usr.user_name, usr.firstname, usr.lastname, rol.role FROM public.chehzam_user usr INNER JOIN public.role rol ON usr.id_role = rol.id_role"
        queryExecutor.execute(sql)
        res = queryExecutor.fetchall()
        users = []
        for id_user, username, firstname, lastname, id_role in res:
            if id_role == 'USER':
                users.append(
                    User(id_user, username, firstname, lastname, Role.USER))
            else:
                users.append(
                    User(id_user, username, firstname, lastname, Role.ADMIN))
        return users

    def get_id(self, id_user: int) -> User:
        queryExecutor = QueryExecutor()
        sql = "SELECT usr.id_user, usr.user_name, usr.firstname, usr.lastname, rol.role FROM public.chehzam_user usr INNER JOIN public.role rol ON usr.id_role = rol.id_role WHERE id_user = %s"
        queryExecutor.execute(sql, (id_user,))
        res = queryExecutor.fetchone()
        if res is not None:
            id_user, username, firstname, lastname, id_role = res
            if id_role == 'USER':
                return User(id_user, username, firstname, lastname, Role.USER)
            return User(id_user, username, firstname, lastname, Role.ADMIN)
        raise NoMatchException()

    def insert(self, user_name: str,  firstname: str, lastname: str, password: str, role: Role) -> User:
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO public.chehzam_user (user_name, firstname, lastname, id_role) VALUES (%s,%s,%s,%s) RETURNING id_user;"
        value = (user_name, firstname, lastname, role.value)
        queryExecutor.cur.execute(sql, value)
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        if res is not None:
            sql = "INSERT INTO public.chehzam_user_password (id_user, password) VALUES (%s,%s)"
            value = (res[0], password)
            queryExecutor.execute(sql, value)
            return User(res[0], user_name, firstname, lastname, role)
        raise NoMatchException()

    def update(self, id_user: int, user_name: str,  firstname: str, lastname: str, role: Role) -> User:
        queryExecutor = QueryExecutor()
        sql = "UPDATE public.chehzam_user SET user_name = %s, firstname = %s, lastname = %s , id_role = %s , WHERE id_user = %s"
        value = (user_name, firstname, lastname, role, id_user)
        queryExecutor.execute(sql, value)
        return User(id_user, user_name, firstname, lastname, role)

    def update_password(self, id_user: int, password: str) -> None:
        queryExecutor = QueryExecutor()
        sql = "UPDATE public.chehzam_user_password SET password = %s WHERE id_user = %s"
        value = (password, id_user)
        queryExecutor.execute(sql, value)

    def delete_id(self, id_user: int) -> None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM public.chehzam_user WHERE id_user = %s"
        queryExecutor.execute(sql, (id_user, ))

    def delete(self) -> None:
        queryExecutor = QueryExecutor()
        sql = "DELETE FROM public.chehzam_user"
        queryExecutor.execute(sql)

    def exist(self, user_name: str, password: str) -> bool:
        queryExecutor = QueryExecutor()
        sql = "SELECT usr.user_name FROM public.chehzam_user usr INNER JOIN public.chehzam_user_password pwd ON usr.id_user = pwd.id_user WHERE usr.user_name = %s AND pwd.password = %s"
        value = (user_name, password)
        queryExecutor.execute(sql, value)
        res = queryExecutor.fetchone()
        if res is None:
            return False
        return True

    def getByUsername(self, user_name: str) -> User | None:
        queryExecutor = QueryExecutor()
        sql = "SELECT usr.id_user, usr.user_name, usr.firstname, usr.lastname, rol.role FROM public.chehzam_user usr INNER JOIN public.role rol ON usr.id_role = rol.id_role WHERE user_name = %s"
        queryExecutor.execute(sql, (user_name,))
        res = queryExecutor.fetchone()
        if res is not None:
            id_user, username, firstname, lastname, id_role = res
            if id_role == 'USER':
                return User(id_user, username, firstname, lastname, Role.USER)
            return User(id_user, username, firstname, lastname, Role.ADMIN)
        raise NoMatchException()

    def get_history(self, id_user: int) -> list[Music]:
        queryExecutor = QueryExecutor()
        sql = "SELECT id_music FROM music_history WHERE id_user = %s ORDER BY HISTORY_DATE DESC"
        queryExecutor.execute(sql, (id_user,))
        res = queryExecutor.fetchall()
        musics = []
        for id_music in res:
            musics.append(MusicGateway().get_id(id_music[0]))
        return musics

    def insert_history(self, history: MusicHistory):
        queryExecutor = QueryExecutor()
        sql = "INSERT INTO music_history (id_user, id_music, history_date) VALUES (%s,%s,%s) RETURNING id_music_history;"
        value = (history.id_user, history.id_music, history.history_date)
        queryExecutor.cur.execute(sql, value)
        res = queryExecutor.cur.fetchone()
        queryExecutor.conn.commit()
        if res is None:
            raise NoMatchException()
        history.id_music_history = res[0]
        return history

    def delete_history(self, id_user: int):
        queryExecutor = QueryExecutor()
        sql = '''DELETE FROM music_history WHERE id_user = %s;'''
        queryExecutor.execute(sql, (id_user,))
