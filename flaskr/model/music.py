from __future__ import annotations
from flaskr.model.album import Album

from flaskr.model.artist import Artist

class Music:
    """
    A class representing a piece of music.

    Attributes
    ----------
    id_music : int, optional
        The id of the music.
    title : str
        The title of the music.
    id_artist : int, optional
        The id of the artist of the music.
    id_album : int, optional
        The id of the album the music belongs to.
        
    Methods
    -------
    serialize()
        Returns a dictionary representation of the music object.
    """

    def __init__(self, id_music: int | None, title: str, artist: Artist | None, featuring: list[Artist] | None, album: Album | None):
        self.id_music: int | None = id_music
        self.title: str = title
        self.artist: Artist | None = artist
        self.featuring: list[Artist] | None = featuring
        self.album: Album | None = album
    
    def serialize(self):
        """
        Returns a dictionary representation of the music object.
        
        Returns
        -------
        dict
            A dictionary containing the id, title, album id and cover id of the music object.
        """

        if self.artist is None or self.album is None:
            raise RuntimeError()
        
        return {
            'id_music': self.id_music,
            'title': self.title,
            'artist': self.artist.serialize(),
            'featuring': [feat.serialize() for feat in self.featuring or []],
            'album': self.album.serialize()
        }
    
