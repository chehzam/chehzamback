import psycopg2

class Connection:
    """
    A class to represent a connection to a PostgreSQL database using psycopg2 library.

    Parameters
    ----------
    user : str, optional
        The user to connect with, by default "postgres".
    password : str, optional
        The password to connect with, by default "".
    host : str, optional
        The hostname of the database server, by default "localhost".
    port : str, optional
        The port number to use for the connection, by default "5432".
    database : str, optional
        The name of the database to connect to, by default "CHEHZAM".

    Attributes
    ----------
    conn : psycopg2.extensions.connection
        The connection object to the PostgreSQL database.

    Raises
    ------
    Exception
        If the connection to the database fails.

    Examples
    --------
    >>> conn = Connection(user="myuser", password="mypassword", database="mydb")
    >>> # use the conn object to query the database
    
    """
    
    def __init__(self, user: str = "postgres", password: str = "", host: str = "localhost", port: str = "5432", database: str = "CHEHZAM"):
        """
        Create a new Connection instance with the specified parameters.

        Parameters
        ----------
        user : str, optional
            The user to connect with, by default "postgres".
        password : str, optional
            The password to connect with, by default "".
        host : str, optional
            The hostname of the database server, by default "localhost".
        port : str, optional
            The port number to use for the connection, by default "5432".
        database : str, optional
            The name of the database to connect to, by default "CHEHZAM".

        Raises
        ------
        Exception
            If the connection to the database fails.

        """
        try:
            self.conn = psycopg2.connect(
                user = user,
                password = password,
                host = host,
                port = port,
                database = database
            )

        # pylint: disable-next = broad-exception-caught
        except (Exception, psycopg2.Error) as error :
            print ("Erreur lors de la connexion à PostgreSQL", error)
  