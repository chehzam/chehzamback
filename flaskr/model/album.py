from __future__ import annotations

from model.cover import Cover

class Album:
    """
    A class representing an album.

    Parameters
    ----------
    id_album : int | None
        The ID of the album.
    title : str
        The title of the album.
    id_cover : int | None
        The ID of the cover image for the album.

    Attributes
    ----------
    id_album : int | None
        The ID of the album.
    title : str
        The title of the album.
    id_cover : int | None
        The ID of the cover image for the album.

    Methods
    -------
    serialize() -> dict
        Returns a dictionary representation of the album object.

    """
    def __init__(self, id_album: int | None, title: str, cover: Cover | None):
        self.id_album: int | None = id_album
        self.title: str = title
        self.cover: Cover | None = cover
    
    def serialize(self):
        """
        Returns a dictionary representation of the album object.

        Returns
        -------
        dict
            A dictionary containing the album's ID, title, and cover image ID.
        """
        if self.cover is None:
            raise RuntimeError()

        return {
            'id_album': self.id_album,
            'title': self.title,
            'cover': self.cover.serialize()
        }
    