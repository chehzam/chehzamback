from __future__ import annotations
import base64

class Cover:
    """
    Class representing a cover image of an album.
    
    Parameters
    ----------
    id_cover : int or None
        The unique identifier of the cover image.
    path : str
        The path to the image file.
    
    Attributes
    ----------
    id_cover : int or None
        The unique identifier of the cover image.
    path : str
        The path to the image file.
    
    Methods
    -------
    serialize() -> dict
        Returns a dictionary representation of the object.
    """

    def __init__(self, id_cover: int | None, image: bytes):
        self.id_cover: int | None = id_cover
        self.image: bytes = image
    
    def serialize(self):
        """
        Returns a dictionary representation of the object.
        
        Returns
        -------
        dict
            A dictionary with keys 'id_cover' and 'path'.
        """
        
        return {
            'id_cover': self.id_cover,
            'image': base64.b64encode(self.image).decode()
        }
