from __future__ import annotations

class Artist:
    """
    A class representing an artist of a book.

    Attributes
    ----------
    id_artist : int or None
        The unique identifier of the artist. None if the artist has not been assigned an ID yet.
    name : str
        The name of the artist.

    Methods
    -------
    serialize()
        Returns a dictionary containing the artist's attributes in a serialized format.

    """
    
    def __init__(self, id_artist: int | None,  name: str):
        self.id_artist: int | None = id_artist
        self.name: str = name
    
    def serialize(self):
        """
        Returns a dictionary containing the artist's attributes in a serialized format.

        Returns
        -------
        dict
            A dictionary containing the artist's attributes in a serialized format.
        """
        return {
            'id_artist': self.id_artist,
            'name': self.name
        }