class Hash:
    """
    A class representing a hash with a fingerprint and a couple.

    Parameters
    ----------
    fingerprint : int
        A fingerprint used to identify the hash.
    couple : int
        A value used to couple the hash.

    Attributes
    ----------
    fingerprint : int
        A fingerprint used to identify the hash.
    couple : int
        A value used to couple the hash.

    """
    
    def __init__(self, fingerprint: int, couple: int)->None:
        self.fingerprint: int = fingerprint
        self.couple: int = couple