from __future__ import annotations
from model.role import Role

class User:
    """
    A class representing a user with an ID, username, first name, last name and role.

    Parameters
    ----------
    id_user : int
        The unique identifier of the user.
    user_name : str
        The username of the user.
    firstname : str
        The first name of the user.
    lastname : str
        The last name of the user.
    role : Role
        The role of the user, represented by an instance of the `Role` enumeration.

    Attributes
    ----------
    id_user : int
        The unique identifier of the user.
    user_name : str
        The username of the user.
    firstname : str
        The first name of the user.
    lastname : str
        The last name of the user.
    role : Role
        The role of the user, represented by an instance of the `Role` enumeration.

    Methods
    -------
    serialize() -> Dict[str, Union[int, str, Dict[str, int]]]
        Returns a dictionary representation of the user object, suitable for serialization to JSON.

    """
    
    def __init__(self, id_user: int, user_name: str,  firstname: str, lastname: str, role: Role):
        self.id_user: int = id_user
        self.user_name: str = user_name
        self.firstname: str = firstname
        self.lastname: str = lastname
        self.role: Role = role
    
    def serialize(self):
        """
        Returns a dictionary representation of the user object, suitable for serialization to JSON.

        Returns
        -------
        dict
            A dictionary containing the user's ID, username, first name, last name and role.
        """
        return {
            'id_user': self.id_user,
            'user_name': self.user_name,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'role': self.role.serialize()
        }