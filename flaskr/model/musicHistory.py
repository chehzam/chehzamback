from __future__ import annotations
from datetime import datetime

class MusicHistory:
    """
    A class representing a music history

    Attributes
    ----------
    id_music_history : int, optional
        The id of the music history.
    id_music : int
        The id of the music.
    id_user : int
        The id of the user of the music.
    history_date : datetime
        The date of registration of the music.
        
    Methods
    -------
    serialize()
        Returns a dictionary representation of the music history object.
    """

    def __init__(self, id_music_history: int | None,  id_music: int, id_user: int, history_date: datetime):
        self.id_music_history: int | None = id_music_history
        self.id_music: int = id_music
        self.id_user: int = id_user
        self.history_date: datetime = history_date

    
    def serialize(self):
        """
        Returns a dictionary representation of the music history object.
        
        Returns
        -------
        dict
            A dictionary containing the id, music id, user id and history date of the music history object.
        """
        
        return {
            'Id music history': self.id_music_history,
            'Id music': self.id_music,
            'Id user': self.id_user,
            'History date': self.history_date
        }
    
