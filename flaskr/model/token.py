from __future__ import annotations

from datetime import datetime, timezone

from model.user import User

class Token:
    """
    A class representing an authentication token.

    Parameters
    ----------
    token : str
        The token string.
    expire_time : datetime
        The expiration time of the token.
    user : User, optional
        The user associated with the token, by default None.

    Attributes
    ----------
    token : str
        The token string.
    expire_time : datetime
        The expiration time of the token.
    User : User, optional
        The user associated with the token, by default None.

    Methods
    -------
    __str__() -> str:
        Returns the token string.
    is_expired() -> bool:
        Returns True if the token is expired, otherwise False.
    serialize() -> dict:
        Returns a dictionary representation of the token.
    """

    def __init__(self, token:str, expire_time:datetime, user:User|None=None):
        self.token: str = token
        self.expire_time: datetime = expire_time
        self.User: User | None = user

    def __str__(self)->str:
        """
        Returns the token string.

        Returns
        -------
        str
            The token string.
        """
        return self.token

    def is_expired(self)->bool:
        """
        Returns True if the token is expired, otherwise False.

        Returns
        -------
        bool
            True if the token is expired, False otherwise.
        """
        return self.expire_time < datetime.now(timezone.utc)
    
    def serialize(self):
        """
        Returns a dictionary representation of the token.

        Returns
        -------
        dict
            A dictionary representation of the token.
        """
        if self.User is None:
            return {
                'token': self.token,
                'expire_time': self.expire_time,
            }
        return {
            'token': self.token,
            'expire_time': self.expire_time,
            'user': self.User.serialize()
        }