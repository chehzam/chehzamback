from enum import Enum


class Role(Enum):
    """
    An enumeration of user roles in the system.

    Attributes:
    -----------
    ADMIN: int
        The value representing the admin role.
    USER: int
        The value representing the user role.
    """

    ADMIN = 1
    USER = 2
    NONE = 3

    def __str__(self):
        """
        Returns the string representation of the role.

        Returns
        -------
        str:
            The string representation of the role.
        """
        return self.name

    @classmethod
    def fromStr(cls, strRole:str):
        """
        Returns the role from the string representation.

        Returns
        -------
        Role:
            The role from the string representation.
        """
        if strRole.upper() == "ADMIN":
            return Role.ADMIN
        elif strRole.upper() == "USER":
            return Role.USER
        else:
            return Role.NONE
        
    def serialize(self):
        """
        Returns the integer value of the role.

        Returns
        -------
        int:
            The integer value of the role.
        """
        return str(self)