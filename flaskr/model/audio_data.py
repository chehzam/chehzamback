import numpy as np
class AudioData():
    """
    A class for representing audio data with associated sampling frequency and duration.
    
    Parameters
    ----------
    data : np.ndarray
        The audio data represented as a numpy array.
    f_sample : float
        The sampling frequency of the audio data.
    normalized : bool, optional
        Whether the audio data is already normalized to the range [-1, 1]. Default is False.
    
    Attributes
    ----------
    data : np.ndarray
        The audio data represented as a numpy array.
    f_sample : float
        The sampling frequency of the audio data.
    normalized : bool
        Whether the audio data is already normalized to the range [-1, 1].
    
    Properties
    ----------
    duration : float
        The duration of the audio data in seconds.
    
    """

    def __init__(self, data: np.ndarray, f_sample: float, normalized: bool = False) -> None:
        self.data: np.ndarray = data
        self.f_sample: float = f_sample
        self.normalized: bool = normalized
    
    @property
    def duration(self) -> float:
        """
        Return the duration of the audio data in seconds.
        
        Returns
        -------
        float
            The duration of the audio data in seconds.
        
        """
        return len(self.data) // self.f_sample
