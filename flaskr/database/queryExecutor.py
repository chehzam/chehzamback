from __future__ import annotations
from database import conn
import psycopg2


class QueryExecutor:
    """
    A class for executing SQL queries on a PostgreSQL database.

    Attributes:
        conn: The connection to the database.
        cur: The cursor for executing SQL queries.

    Methods:
        __init__: Initializes the QueryExecutor class.
        execute: Executes an SQL query.
        fetchall: Retrieves all results from the last executed query.
        fetchone: Retrieves the next result from the last executed query.
        lastrowid: Retrieves the ID of the last inserted row.
        __del__: Closes the cursor when it is deleted.
    """

    def __init__(self)->None:
        """
        Initializes the QueryExecutor class.

        Args:
            None

        Returns:
            None
        """
        self.conn = conn
        self.cur = conn.cursor()
    
    def execute(self, sql:str, values=()):
        """
        Executes an SQL query.

        Args:
            sql: The SQL query to execute.
            values: Optional parameter for query parameters.

        Returns:
            None
        """
        try:
            if values:
                self.cur.execute(sql, values)
                self.conn.commit()
            else:
                self.cur.execute(sql)
                self.conn.commit()
                
        # pylint: disable-next = broad-exception-caught
        except (Exception, psycopg2.Error) as error:
            print(error)
            self.conn.rollback()
    
    def fetchall(self):
        """
        Retrieves all results from the last executed query.

        Args:
            None

        Returns:
            All results from the last executed query.
        """
        return self.cur.fetchall()
    
    def fetchone(self):
        """
        Retrieves the next result from the last executed query.

        Args:
            None

        Returns:
            The next result from the last executed query.
        """
        return self.cur.fetchone()
    
    def lastrowid(self):
        """
        Retrieves the ID of the last inserted row.

        Args:
            None

        Returns:
            The ID of the last inserted row.
        """
        return self.cur.lastrowid
    
    def __del__(self):
        """
        Closes the cursor when it is deleted.

        Args:
            None

        Returns:
            None
        """
        self.cur.close()