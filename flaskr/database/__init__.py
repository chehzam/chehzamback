import os
import psycopg2

try:
    conn = psycopg2.connect(
        user="postgres",
        password="admin",  # To change
        # Get from env variable or localhost
        host=os.environ.get('DATABASE_HOST', 'localhost'),
        port="5432",
        database="CHEHZAM"
    )

# pylint: disable-next = broad-exception-caught
except (Exception, psycopg2.Error) as error:
    print("Erreur lors de la connexion à PostgreSQL", error)