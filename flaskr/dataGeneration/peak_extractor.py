from __future__ import annotations
import numpy as np
from scipy import signal

from .spectrogram import Spectrogram
from .peak import Peak

class PeakExtractor():
    """
    Extract peaks from a Spectrogram object.

    Parameters
    ----------
    None

    Attributes
    ----------
    spectrogram : Spectrogram or None
        The spectrogram to extract peaks from.

    Methods
    -------
    extract(nb_of_bands=6)
        Extract peaks from the spectrogram.
    extract_spectrum(x)
        Extract a spectrum from the spectrogram.

    Raises
    ------
    RuntimeError
        If `spectrogram` is not set before calling any method.
    """
    

    def __init__(self) -> None:
        self.spectrogram: Spectrogram | None = None

    def extract(self, nb_of_bands: int = 6) -> list[Peak]:
        """
        Extract peaks from the spectrogram.

        Parameters
        ----------
        nb_of_bands : int, optional
            Number of frequency bands to group together (default is 6).

        Returns
        -------
        list[Peak]
            List of peaks.

        Raises
        ------
        RuntimeError
            If `spectrogram` is not set before calling this method.
        """
        if isinstance(self.spectrogram, Spectrogram):
            sxx = self.spectrogram.Sxx
            freq = self.spectrogram.f
            time = self.spectrogram.t
            sxx_trans = sxx.transpose()

            list_of_peaks = list()
            
            for i, n in enumerate(sxx_trans):
                idx, _ = signal.find_peaks(
                    np.log10(n) * 20,
                    distance=n.shape[0]//nb_of_bands)
                for j in idx:
                    list_of_peaks.append(Peak(freq[j], time[i], sxx[j][i]))
            
            return list_of_peaks
        raise RuntimeError("self.spectrogram must be instanciate.")

    def extract_spectrum(self, x: int) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Extract a spectrum from the spectrogram.

        Parameters
        ----------
        x : int
            Index of the time axis.

        Returns
        -------
        tuple[np.ndarray, np.ndarray, np.ndarray]
            Tuple of frequency, power, and peak indices.

        Raises
        ------
        RuntimeError
            If `spectrogram` is not set before calling this method.
        """
        spectrogram = self.spectrogram
        if isinstance(spectrogram, Spectrogram):
            sxx = spectrogram.Sxx
            freq = spectrogram.f
            sxx_trans = sxx.transpose()
            
            t = 20*np.log10(sxx_trans[x])
            idx, _ = signal.find_peaks(t, distance=sxx_trans.shape[1]//6)
            
            return (freq, sxx_trans[x], idx)
        raise RuntimeError("self.spectrogram must be instanciate.")
