from __future__ import annotations
import matplotlib.pyplot as plt
import numpy as np
from model.audio_data import AudioData
from .spectrogram_generator import SpectrogramGenerator
from .peak_extractor import PeakExtractor
from .hash_generator import HashGenerator

class DataGenerator():
    """
    Class DataGenerator:
        A class responsible for generating data from audio files.

    Attributes:
        audio_data (AudioData | None): The audio data object.
        spec_generator (SpectrogramGenerator): The spectrogram generator.
        peak_extractor (PeakExtractor): The peak extractor.
        hash_generator (HashGenerator): The hash generator.
    
    Methods:
        __init__(): Initializes an instance of the DataGenerator class. 
        generate() -> dict[str, int]: Generates hashes from the audio data.
        export_spectrogram(title: str, export_path: str): Exports a spectrogram image of the audio data.
        export_peaks(title: str, export_path: str) -> None: Exports a scatter plot of the extracted peaks.
        export_peaks_spectrum(title: str, export_path: str) -> None: Exports a plot of the spectrum with highlighted peaks.
        export_hash(title: str, export_path: str) -> None: Exports the generated hash to a file.
        get_hash() -> dict[str, int]: Retrieves the generated hash.
    """

    def __init__(self) -> None:
        """
        Initializes the QueryExecutor class.

        Args:
            None

        Returns:
            None
        """
        self.audio_data: AudioData | None = None
        self.spec_generator: SpectrogramGenerator = SpectrogramGenerator(
            512,
            512 - 128
        )
        self.peak_extractor: PeakExtractor = PeakExtractor()
        self.hash_generator: HashGenerator = HashGenerator(10, 0)

    def generate(self) -> dict[str, int]:
        """
        Generates hashes from the audio data.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        hashes : dict[str, int]
            A dictionary containing the generated hashes and their corresponding start times.
            
        Raises
        ------
        RuntimeError
            If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spec = self.spec_generator.generate_spectrogram()
                self.peak_extractor.spectrogram = spec
                peaks = self.peak_extractor.extract()
                self.hash_generator.peaks = peaks
                return self.hash_generator.generate_hash(min_time_delta=0.00001, max_time_delta=20)
            raise RuntimeError("self.audio_data must be instanciate.")
        except RuntimeError as e:
            raise e

    # pylint: disable-next=redundant-returns-doc
    def export_spectrogram(self, title: str, export_path: str):
        """
        Exports a spectrogram image of the audio data.

        Parameters:
            title: str
                The title of the exported image.
            export_path: str
                The path to export the image.

        Returns:
            None

        Raises:
            RuntimeError: If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spectrogram = self.spec_generator.generate_spectrogram()

                t = spectrogram.t
                f = spectrogram.f
                Sxx = spectrogram.Sxx

                cmap = plt.get_cmap('rainbow') # type: ignore
                cmap.set_under(color='k', alpha=None)
                fig, ax = plt.subplots(1, 1)
                fig.set_size_inches(16, 9)
                img = ax.pcolormesh(t, f, 20*np.log10(Sxx), cmap=cmap)
                ax.set(xlabel="Temps (s)", ylabel="Fréquence (Hz)", title=title)
                cbar = fig.colorbar(img)
                cbar.set_label("Densité spectrale d'énergie (dB)")
                plt.savefig(f"{export_path}/{title}.png")
        except RuntimeError as e:
            raise e

    # pylint: disable-next=redundant-returns-doc
    def export_peaks(self, title: str, export_path: str) -> None:
        """
        Exports a scatter plot of the extracted peaks.

        Parameters:
            title: str
                The title of the exported plot.
            export_path: str
                The path to export the plot.

        Returns:
            None

        Raises:
            RuntimeError: If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spec = self.spec_generator.generate_spectrogram()
                self.peak_extractor.spectrogram = spec
                peaks = self.peak_extractor.extract()
                x, y = list(), list()
                for el in peaks:
                    x.append(el.time)
                    y.append(el.frequency)
                
                fig, ax = plt.subplots(1, 1)
                fig.set_size_inches(16, 9)

                ax.scatter(x, y, marker="x") # type: ignore
                ax.set(xlabel="Temps (s)", ylabel="Fréquence (Hz)", title=title)
                plt.savefig(f"{export_path}/{title}_peaks_scatter.png")
        except RuntimeError as e:
            raise e
    
    # pylint: disable-next=redundant-returns-doc
    def export_peaks_spectrum(self, title: str, export_path: str) -> None:
        """
        Exports a plot of the spectrum with highlighted peaks.

        Parameters:
            title: str
                The title of the exported plot.
            export_path: str
                The path to export the plot.

        Returns:
            None

        Raises:
            RuntimeError: If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spec = self.spec_generator.generate_spectrogram()
                self.peak_extractor.spectrogram = spec
                x, y, idx = self.peak_extractor.extract_spectrum(200)
                fig, _ = plt.subplots(1, 1)
                fig.set_size_inches(16, 9)

                plt.plot(x, y)

                plt.scatter(np.array(x)[idx], np.array(y)[idx], c="r", marker="x") # type: ignore
                
                plt.savefig(f"{export_path}/{title}_peaks_spectrum.png")
            else:
                raise RuntimeError("self.__audio_data must be instanciate.")
        except RuntimeError as e:
            raise e

    # pylint: disable-next=redundant-returns-doc
    def export_hash(self, title: str, export_path: str) -> None:
        """
        Exports the generated hash to a file.

        Parameters:
            title: str
                The title of the exported hash file.
            export_path: str
                The path to export the hash file.

        Returns:
            None

        Raises:
            RuntimeError: If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spec = self.spec_generator.generate_spectrogram()
                self.peak_extractor.spectrogram = spec
                peaks = self.peak_extractor.extract()
                self.hash_generator.peaks = peaks
                hashes = self.hash_generator.generate_hash(min_time_delta=0.00001, max_time_delta=20)
                with open(f"{export_path}/{title}_hash.log", "w+", encoding="utf-8") as F:
                    for el in hashes:
                        F.write(f"{el}\n")
            else:
                raise RuntimeError("self.audio_data must be instanciate.")
        except RuntimeError as e:
            raise e
        
    def get_hash(self) -> dict[str, int]:
        """
        Retrieves the generated hash.

        Returns:
            hashes : dict[str, int]
                A dictionary containing the generated hashes and their corresponding start times.

        Raises:
            RuntimeError: If `audio_data` has not been instantiated.
        """
        try:
            if self.audio_data is not None:
                self.spec_generator.audio_data = self.audio_data
                spec = self.spec_generator.generate_spectrogram()
                self.peak_extractor.spectrogram = spec
                peaks = self.peak_extractor.extract()
                self.hash_generator.peaks = peaks
                return self.hash_generator.generate_hash(min_time_delta=0.00001, max_time_delta=20)
            raise RuntimeError("self.__audio_data must be instanciate.")
        except RuntimeError as e:
            raise e
