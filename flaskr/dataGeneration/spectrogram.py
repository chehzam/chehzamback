import numpy as np
class Spectrogram():
    """A class representing the spectrogram of an audio signal.

    Attributes:
        f (numpy.ndarray): Array of sample frequencies.
        t (numpy.ndarray): Array of segment times.
        Sxx (numpy.ndarray): Spectrogram of the audio signal.
    """

    def __init__(self, f: np.ndarray, t: np.ndarray, Sxx: np.ndarray) -> None:
        """Initialize a new instance of Spectrogram class.

        Args:
            f (numpy.ndarray): Array of sample frequencies.
            t (numpy.ndarray): Array of segment times.
            Sxx (numpy.ndarray): Spectrogram of the audio signal.
        """
        self.f: np.ndarray = f
        self.t: np.ndarray = t
        self.Sxx: np.ndarray = Sxx