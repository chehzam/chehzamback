from __future__ import annotations
from .peak import Peak


class HashGenerator():
    """
    A class that generates hashes from a list of `Peak` objects.

    Parameters
    ----------
    nb_of_anchors : int
        The number of anchors to use in each hash.
    target_zone_size : int
        The size of the target zone (in Hz) to use when generating hashes.

    Attributes
    ----------
    peaks : list of Peak objects or None
        The list of `Peak` objects from which to generate hashes.
    nb_of_anchors : int
        The number of anchors to use in each hash.
    target_zone_size : int
        The size of the target zone (in Hz) to use when generating hashes.

    Methods
    -------
    generate_hash(min_time_delta: float, max_time_delta: float) -> Dict[str, int]: Generates hashes for the list of `Peak` objects.
    """

    def __init__(self, nb_of_anchors: int, target_zone_size: int) -> None:
        """Initialize the HashGenerator.

        Args:
            nb_of_anchors (int): The number of anchor points used in generating the hashes.
            target_zone_size (int): The size of the target zone used in generating the hashes.

        Returns:
            None

        """
        self.peaks: list[Peak] | None = None
        self.nb_of_anchors: int = nb_of_anchors
        self.target_zone_size: int = target_zone_size

    def generate_hash(self, min_time_delta: float, max_time_delta: float) -> dict[str, int]:
        """
        Generates hashes for the list of `Peak` objects.

        Parameters
        ----------
        min_time_delta : float
            The minimum time delta (in seconds) between two `Peak` objects to include in the hash.
        max_time_delta : float
            The maximum time delta (in seconds) between two `Peak` objects to include in the hash.

        Returns
        -------
        hashes : dict of str and int
            The generated hashes, where the keys are string representations of the hash IDs and the values are the
            corresponding starting times (in milliseconds).
            
        Raises
        ------
        RuntimeError
            If `self.peaks` is not instantiated.
        """
        hashes = dict()
        peaks = self.peaks
        if peaks is not None:
            for anchor_index, anchor in enumerate(peaks):
                for target_index in range(self.nb_of_anchors):
                    if anchor_index + target_index < len(peaks):
                        anchor_freq = int(anchor.frequency)
                        target_freq = int(peaks[anchor_index+target_index].frequency)
                        anchor_time = anchor.time
                        target_time = peaks[anchor_index+target_index].time
                        delta_time = target_time - anchor_time
                        if min_time_delta <= delta_time <= max_time_delta:
                            int_delta_time = int(delta_time*1000)
                            hash_id = int(f"{int(target_freq):016b}" + f"{int(anchor_freq):016b}" + f"{int_delta_time:032b}", 2)
                            hash_start = int(anchor_time * 1000)
                            hashes[str(hash_id)] = hash_start
            return hashes
        raise RuntimeError("self.peaks must be instanciate.")
