class Peak():
    """
    A peak in a spectrogram.

    Parameters
    ----------
    frequency : int
        The frequency of the peak.
    time : int
        The time of the peak.
    value : float
        The value of the peak.

    Attributes
    ----------
    frequency : int
        The frequency of the peak.
    time : int
        The time of the peak.
    value : float
        The value of the peak.
    id : int
        The ID of the peak.

    Notes
    -----
    The ID is assigned automatically and incremented each time a peak is created.
    """
    id = 0

    def __init__(self, frequency: int, time: int, value: float) -> None:
        self.frequency: int = frequency
        self.time: int = time
        self.value: float = value
        self.id: int = Peak.id
        Peak.id += 1