from __future__ import annotations
from scipy import signal
from model.audio_data import AudioData
from .spectrogram import Spectrogram
from .window_selector import WindowSelector


class SpectrogramGenerator():
    """
    Generate a spectrogram from an audio signal using the short-time Fourier transform.

    Parameters
    ----------
    nb_per_seg: int
        The number of samples per segment.
    nb_overlap: int
        The number of samples of overlap between segments.

    Attributes
    ----------
    audio_data: AudioData | None
        The audio data to generate the spectrogram from.
    nb_per_seg: int
        The number of samples per segment.
    nb_overlap: int
        The number of samples of overlap between segments.
    selector: WindowSelector
        The window selector used to choose the window function.

    Methods
    -------
    generate_spectrogram() -> Spectrogram
        Generate the spectrogram from the audio data.

    Raises
    ------
    RuntimeError
        If self.audio_data is not instanciated.

    Notes
    -----
    The spectrogram represents the time-varying spectrum of a signal. It is computed using the
    short-time Fourier transform, which computes the Fourier transform of overlapping segments
    of the signal. The resulting spectrogram has time on the x-axis, frequency on the y-axis,
    and amplitude represented by a color intensity.
    """

    def __init__(self, nb_per_seg: int, nb_overlap: int) -> None:
        self.audio_data: AudioData | None = None
        self.nb_per_seg: int = nb_per_seg
        self.nb_overlap: int = nb_overlap
        self.selector: WindowSelector = WindowSelector()

    def generate_spectrogram(self) -> Spectrogram:
        """
        Generate the spectrogram from the audio data.

        Returns
        -------
        Spectrogram
            The spectrogram of the audio data.

        Raises
        ------
        RuntimeError
            If self.audio_data is not instanciated.
        """
        audio_data = self.audio_data
        if isinstance(audio_data, AudioData):
            fe_reel = audio_data.f_sample
            nperseg = self.nb_per_seg
            noverlap = self.nb_overlap
            window = self.selector.select_window("hann", nperseg)

            f, t, Sxx = signal.spectrogram(
                x=audio_data.data,
                fs=fe_reel,
                window=window,
                nperseg=nperseg,
                noverlap=noverlap,
                nfft=None)
            return Spectrogram(f, t, Sxx)
        raise RuntimeError("self.audio_data must be instanciate.")
