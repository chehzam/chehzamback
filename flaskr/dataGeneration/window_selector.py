"""
Module: window_selector

This module provides the WindowSelector class for selecting windows for signal processing.

Classes:
    WindowSelector:
        A class responsible for selecting windows for signal processing.

Imported Dependencies:
    from __future__ import annotations
    from scipy import signal
"""

from __future__ import annotations
from scipy import signal

class WindowSelector():
    """
    WindowSelector class for selecting windows for signal processing.

    Methods:
        select_window(window, n_x, fftbins: bool = True): Selects a window for signal processing.
    """

    def select_window(self, window, n_x, fftbins: bool = True):
        """
        Selects a window for signal processing.

            Parameters:
                window: The type of window to select.
                n_x: The number of samples in the window.
                fftbins (bool): A flag indicating whether to return FFT bins or not. Default is True.

            Returns:
                selected_window: The selected window.
        """
        return signal.get_window(window, n_x, fftbins)
        #return Window.HAMMING_WINDOW.value
