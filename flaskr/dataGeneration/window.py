"""
Module: window

This module provides an enumeration class for window types used in signal processing.

Classes:
    Window:
        An enumeration class representing different window types.

Imported Dependencies:
    from enum import Enum

Usage:
    import window

    # Access window types
    hamming_window = window.Window.HAMMING_WINDOW
    rectangular_window = window.Window.RECTANGULAR
    blackman_window = window.Window.BLACKMAN_WINDOW
"""

from enum import Enum
class Window(Enum):
    """
    Window enumeration class representing different window types used in signal processing.

    Enumeration Values:
        HAMMING_WINDOW: Represents the Hamming window.
        RECTANGULAR: Represents the rectangular window.
        BLACKMAN_WINDOW: Represents the Blackman window.
    """

    HAMMING_WINDOW = "hamming"
    RECTANGULAR = "boxcar"
    BLACKMAN_WINDOW = "blackman"
