from __future__ import annotations

import os
from os import listdir
from io import BytesIO
import soundfile as sf
from tempfile import NamedTemporaryFile
from tinytag import TinyTag
from model.audio_data import AudioData
from model.album import Album
from model.cover import Cover
from model.artist import Artist
from model.music import Music
from model.gateway.hashGateway import HashGateway
from model.gateway.artistGateway import ArtistGateway
from model.gateway.albumGateway import AlbumGateway
from model.gateway.musicGateway import MusicGateway
from model.gateway.coverGateway import CoverGateway
from dataModifiers.audio_modifier import AudioModifier
from dataGeneration.data_generator import DataGenerator
from .analyse_manager import AnalyseManager
from exception.noMatchExeption import NoMatchException

class Manager():
    """Manage audio files and their associated metadata.

    This class provides methods to identify audio samples, import songs from a
    directory, and retrieve information about the songs and their associated
    albums, artists, and cover images.

    Attributes:
        __analyse_manager (AnalyseManager): An instance of the `AnalyseManager`
            class used to analyze audio data.

    Methods:
        identify_sample(audio_data): Identify a sample of audio data and return
            information about the associated song as a dictionary.
        get_song(song_id): Retrieve information about a song with the given ID
            as a dictionary.
        open_file(path, start=0, end=-1): Open an audio file at the given path,
            return its audio data as an instance of the `AudioData` class, and
            optionally specify a range of samples to extract.
        import_songs(path): Import all audio files with the `.flac` extension
            from the directory at the given path and add their associated
            metadata to the database.

    Raises:
        RuntimeError: If an album, cover, or author is not found for a song.
        """

    def __init__(self) -> None:
        self.__analyse_manager = AnalyseManager()
        

    def identify_sample(self, audio_data: AudioData) -> dict:
        """
        Identifies a song given an AudioData object.

        Args:
            audio_data (AudioData): An instance of AudioData representing an audio clip.

        Returns:
            A dictionary containing the song's metadata.
        """
        id_music = self.__analyse_manager.start_analyse(audio_data)
        return self.get_song(id_music)

    def get_song(self, song_id: int) -> dict:
        """
        Retrieves a song's metadata given its ID.

        Args:
            song_id (int): The ID of the song.

        Returns:
            A dictionary containing the song's metadata.
        """
        music_gateway = MusicGateway()
        try:
            music = music_gateway.get_id(song_id)
            return music.serialize()
        except NoMatchException as e:
            raise e

    def add_song(self, data: bytes) -> None:
        """_summary_

        Args:
            data (bytes): _description_
        """
        tmp = NamedTemporaryFile(delete=False)
        try:
            tmp.write(data)
            metadata = self.get_metadata_from_flac(tmp.name)
            id_music = self.write_metadata(metadata)
            self.write_fingerprint(tmp.name, id_music)
        finally:
            tmp.close()
            os.unlink(tmp.name)
    
    def get_metadata_from_flac(self, path: str) -> dict:
        """_summary_

        Args:
            path (str): _description_

        Raises:
            e: _description_

        Returns:
            dict: _description_
        """
        audio = TinyTag.get(path, image= True)
        metadata = dict()
        metadata["cover"] = audio.get_image()
        metadata["album_name"] = audio.album
        metadata["music_name"] = audio.title
        metadata["featuring"] = []
        if audio.artist is not None:
            artist_list = audio.artist.split(", ")
            metadata["artist"] = artist_list[0]
            if len(artist_list) > 1:
                for element in artist_list[1:]:
                    metadata["featuring"].append(element.strip())
        return metadata
    
    def write_metadata(self, metadata: dict) -> int:
        """_summary_

        Args:
            metadata (dict): _description_

        Raises:
            RuntimeError: _description_

        Returns:
            int: _description_
        """
        # instantiation of gateways
        album_gateway = AlbumGateway()
        cover_gateway = CoverGateway()
        music_gateway = MusicGateway()
        author_gateway = ArtistGateway()

        # Creation of Models
        artist = Artist(None, metadata["artist"])
        album = Album(id_album=None, title=metadata["album_name"], cover=None)
        cover = Cover(id_cover=None, image=metadata["cover"])
        music = Music(id_music=None, title=metadata["music_name"], artist=None, featuring=None, album=None)
        featuring = [Artist(None, feat.strip()) for feat in metadata["featuring"]]

        # Creation of main artist if does not exists
        artist = author_gateway.get_by_name(artist)
        if artist.id_artist is None:
            artist = author_gateway.insert(artist)

        # Creation of artists in DB if don't exist
        for i, feat in enumerate(featuring):
            feat = author_gateway.get_by_name(feat)
            if feat.id_artist is None:
                feat = author_gateway.insert(feat)
            featuring[i] = feat

        # Search if an artist with the same name exists
        artist = author_gateway.get_by_name(artist)
        if artist is None:
            artist = Artist(None, artist.name)
            artist = author_gateway.insert(artist)
        
        # Search if an album with the same name exists for a given artist
        album = album_gateway.get_by_titre(album, artist)
        if album.id_album is None:
            cover = cover_gateway.insert(cover)
            album.cover = cover
            album = album_gateway.insert(album)
        
        # Search if a music exists for a given album
        music.album = album
        music.artist = artist
        music.featuring = featuring
        music = music_gateway.get_by_title(music)
        if music.id_music is None:
            music = music_gateway.insert(music)
        
        if music.id_music is None:
            raise RuntimeError()
        
        return music.id_music

    def write_fingerprint(self, file_path: str, id_music: int) -> None:
        """_summary_

        Args:
            file_path (str): _description_
            id_music (int): _description_
        """
        gateway = HashGateway()
        data_generator = DataGenerator()
        audio_modifier = AudioModifier()
        audio_data = self.open_file(file_path)
        audio_modifier.audio_data = audio_data
        data_generator.audio_data = audio_modifier.normalize()
        hashing = data_generator.get_hash()
        data = []
        for key, val in hashing.items():
            couple = int(f"{int(val):032b}" + f"{int(id_music):032b}", 2)
            data.append((int(key), couple))
        gateway.insertHashs(data)
    
    def open_file(self, path: str, start: int = 0, end: int = -1) -> AudioData:
        """
        Reads an audio file given its path, and returns an AudioData object representing the audio clip.

        Args:
            path (str): The path to the audio file.
            start (int): The start time of the audio clip (in seconds). Defaults to 0.
            end (int): The end time of the audio clip (in seconds). Defaults to -1 (i.e., the end of the file).

        Returns:
            An instance of AudioData representing the audio clip.
        """
        data_reel, fe_reel = sf.read(path, dtype="int16")
        if len(data_reel.shape) == 2:
            x = data_reel.sum(axis=1) / 2
        else:
            x = data_reel
        return AudioData(x[start*fe_reel:-1 if end == -1 else end*fe_reel], fe_reel)
    
    def open_file_from_bytes(self, bytes_data: bytes, start: int = 0, end: int = -1) -> AudioData:
        """
        Reads an audio file given its path, and returns an AudioData object representing the audio clip.

        Args:
            bytes_data (bytes): audio data as bytes.
            start (int): The start time of the audio clip (in seconds). Defaults to 0.
            end (int): The end time of the audio clip (in seconds). Defaults to -1 (i.e., the end of the file).

        Returns:
            An instance of AudioData representing the audio clip.
        """
        data_reel, fe_reel = sf.read(BytesIO(bytes_data), dtype="int16")
        if len(data_reel.shape) == 2:
            x = data_reel.sum(axis=1) / 2
        else:
            x = data_reel
        return AudioData(x[start*fe_reel:-1 if end == -1 else end*fe_reel], fe_reel)
    
    def import_songs(self, path: str) -> None:
        """
        Imports songs from a given directory, and adds them to the database.

        Args:
            path (str): The path to the directory containing the audio files.
        """
        onlyfiles = [f for f in listdir(path) if f[-5:] == ".flac"]
        for file in onlyfiles:
            metadata = self.get_metadata_from_flac(f"{path}/{file}")
            
            id_music = self.write_metadata(metadata)

            print(f"importing {file}...\t", end="")
            self.write_fingerprint(f"{path}/{file}", id_music)
            print(f"{file} imported")

