from __future__ import annotations
from scipy.signal import find_peaks
from model.audio_data import AudioData
from model.gateway.hashGateway import HashGateway
from dataGeneration.data_generator import DataGenerator
from dataModifiers.audio_modifier import AudioModifier
from exception.noMatchExeption import NoMatchException


class AnalyseManager():
    """
    A class used to handle audio data, generate audio data, modify audio data, and analyze audio data using fingerprinting
    techniques.

    Attributes:
        hashs_match (dict[int, dict] | None): A dictionary containing the matching audio data's fingerprints and hashes.
        hashs_sample (dict[str, int] | None): A dictionary containing the audio data's fingerprints and hashes.
        audio_data (AudioData | None): An object representing audio data.
        data_generator (DataGenerator): An object used to generate audio data.
        audio_modifier (AudioModifier): An object used to modify audio data.
        recognizer (Recognizer): An object used to recognize audio data.

    Methods:
        set_audio_data(audio_data: AudioData) -> None: Sets the audio data to the given audio data.
        modify_audio() -> None: Modifies the audio data.
        generate_data() -> dict[str, int]: Generates audio data.
        start_analyse(data: AudioData) -> int: Starts analyzing the audio data.
        fetch_matches() -> dict: Fetches the matches of the audio data.
        matching_limiter(threshold: float = 20.0) -> None: Limits the matches of the audio data.
        get_time_coherency() -> int: Gets the time coherency of the audio data.
    """

    def __init__(self) -> None:
        self.hashs_match: dict[int, dict] | None = None
        self.hashs_sample: dict[str, int] | None = None
        self._audio_data: AudioData | None = None
        self.data_generator: DataGenerator = DataGenerator()
        self.audio_modifier: AudioModifier = AudioModifier()

    @property
    def audio_data(self) -> AudioData | None:
        """Getter for the audio data.

        Returns:
            AudioData | None: The audio data being modified.
        """
        return self._audio_data

    @audio_data.setter
    def audio_data(self, audio_data: AudioData):
        """Setter for the audio data.

        Args:
            audio_data (AudioData): The audio data to modify.
        """
        self._audio_data = audio_data
        self.data_generator.audio_data = audio_data
        self.audio_modifier.audio_data = audio_data

    def modify_audio(self) -> None:
        """
        Applies audio processing to have a standart type of audio data.

        Raises:
            RuntimeError: If audio data is not set.
        """
        try:
            new_data = self.audio_modifier.normalize()
            self.audio_data = new_data
        except RuntimeError as e:
            raise e

    def generate_data(self) -> dict[str, int]:
        """
        Generates fingerprints hashes of a audio data.

        Raises:
            RuntimeError: If audio data is not set.

        Returns:
            A dictionary containing the audio data's fingerprints and hashes.
        """
        try:
            return self.data_generator.generate()
        except RuntimeError as e:
            raise e

    def start_analyse(self, data: AudioData) -> int:
        """Starts the analysis of the audio data.

        Args:
            data (AudioData): The audio data to analyze.

        Returns:
            int: The ID of the music that best matches the audio data.
        """
        self.audio_data = data
        self.modify_audio()
        self.hashs_sample = self.data_generator.generate()
        self.hashs_match = self.fetch_matches()
        self.matching_limiter(60.0)
        return self.get_time_coherency()

    def fetch_matches(self) -> dict:
        """Fetches the matches for the audio data from the hash gateway.

        Returns:
            dict: A dictionary of the music IDs that have matching audio samples, 
                with a dictionary of the start times of the matched audio samples 
                for each music ID.
        Raises:
            RuntimeError: If self.hashs_sample is not initialized.
        """
        if self.hashs_sample is None:
            raise RuntimeError("self.self.hashs_sample must be instanciated.")

        hash_gateway = HashGateway()
        matching = dict()
        tuple_of_fingerprint = tuple(self.hashs_sample.keys())
        matching_list = hash_gateway.get(tuple_of_fingerprint)
        for hash_match in matching_list:
            anchor_start = self.hashs_sample[str(hash_match.fingerprint)]
            match_couple = f"{int(hash_match.couple):064b}"
            music_id = int(match_couple[32:], 2)
            result_start = int(match_couple[:32], 2)
            if music_id not in matching:
                matching[music_id] = dict()
            else:
                if result_start - anchor_start not in matching[music_id]:
                    matching[music_id][result_start - anchor_start] = 1
                else:
                    matching[music_id][result_start - anchor_start] += 1
        return matching

    def matching_limiter(self, threshold: float = 50.0) -> None:
        """Limits the matches to those that exceed the threshold percentage of 
        the total number of audio samples.

        Args:
            threshold (float, optional): The threshold percentage to limit the 
                matches to. Defaults to 50.0.

        Raises:
            RuntimeError: If self.hashs_sample or self.hashs_match are not initialized.
        """
        if self.hashs_sample is None:
            raise RuntimeError("self.self.hashs_sample must be instanciated.")
        elif self.hashs_match is None:
            raise RuntimeError("self.self.hashs_match must be instanciated.")

        matching = dict()
        for music_id, matches in self.hashs_match.items():
            if 100 * sum(matches.values()) / len(self.hashs_sample) >= threshold:
                matching[music_id] = matches
        if len(matching.keys()) == 0:
            raise NoMatchException()
        self.hashs_match = matching

    def get_time_coherency(self) -> int:
        """Calculate the time coherency of the audio sample with the matched audio data.

        Returns:
            int: The music ID with the maximum time coherency.

        Raises:
            RuntimeError: If self.hashs_match is not instantiated.
        """
        if self.hashs_match is None:
            raise RuntimeError("self.self.hashs_match must be instanciated.")

        max_coherency = (-1, -1)
        for music_id, matches in self.hashs_match.items():
            data = dict(sorted(matches.items(), key=lambda el: el[0]))
            _, [*values] = zip(*data.items())
            peak, _ = find_peaks(values, distance=len(values))
            peak_value = values[max(peak)]
            if peak_value > max_coherency[1]:
                max_coherency = (music_id, peak_value)
        if max_coherency == (-1 , -1):
            raise NoMatchException()
        return max_coherency[0]
