from __future__ import annotations
import base64
import soundfile as sf
from flask import request
from flask_api import status
from gestion.manager.manager import Manager
from model.cover import Cover
from model.musicHistory import MusicHistory
from model.album import Album
from model.audio_data import AudioData
from model.artist import Artist
from model.music import Music
from model.role import Role
from model.user import User
from model.gateway.albumGateway import AlbumGateway
from model.gateway.artistGateway import ArtistGateway
from model.gateway.coverGateway import CoverGateway
from model.gateway.hashGateway import HashGateway
from model.gateway.musicGateway import MusicGateway
from model.gateway.userGateway import UserGateway
from model.gateway.tokenGateway import TokenGateway
from main import routesBp
from api.apiAccessRigth import ApiAccessRigth
from api.apiAuth import ApiAuth
from exception.noMatchExeption import NoMatchException
from .doc import auto



rigthVerif = ApiAccessRigth()
authentificator = ApiAuth()
tokenGateway = TokenGateway()
artistGateway = ArtistGateway()
albumGateway = AlbumGateway()
coverGateway = CoverGateway()
hashGateway = HashGateway()
musicGateway = MusicGateway()
userGateway = UserGateway()



########################
###### API routes ######
########################

###### user routes ######
@routesBp.route('/user', methods=['GET'])
@auto.doc('public')
def get_users():
    """Get all users"""
    if rigthVerif.checkAccessRigth("/user", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(userGateway.get())
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/user/<int:id_user>', methods=['GET'])
@auto.doc('public')
def get_user(id_user):
    """Get a user by id"""
    if rigthVerif.checkAccessRigth("/user/x", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        try:
            return userGateway.get_id(id_user).serialize()
        except RuntimeError:
            return "Not found", status.HTTP_404_NOT_FOUND
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/user/<int:id_user>', methods=['PUT'])
@auto.doc('public')
def update_user(id_user):
    """Update a user by id"""
    if rigthVerif.checkAccessRigth("/user/x", "PUT", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        user = getUser()
        return userGateway.update(id_user, user.user_name, user.firstname, user.lastname, user.role).serialize()
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/user', methods=['DELETE'])
@auto.doc('public')
def delete_users():
    """Delete all users"""
    if rigthVerif.checkAccessRigth("/user", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        userGateway.delete()
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/user/<int:id_user>', methods=['DELETE'])
@auto.doc('public')
def delete_user(id_user):
    """Delete a user by id"""
    if rigthVerif.checkAccessRigth("/user/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        userGateway.delete_id(id_user)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


###### cover routes ######
@routesBp.route('/cover', methods=['GET'])
@auto.doc('public')
def get_covers():
    """Get all covers"""
    if rigthVerif.checkAccessRigth("/cover", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(coverGateway.get())
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/cover/<int:id_cover>', methods=['GET'])
@auto.doc('public')
def get_cover(id_cover):
    """Get a cover by id"""
    if rigthVerif.checkAccessRigth("/cover/x", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        try:
            return coverGateway.get_id(id_cover).serialize()
        except RuntimeError:
            return "Not found", status.HTTP_404_NOT_FOUND
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/cover', methods=['DELETE'])
@auto.doc('public')
def delete_covers():
    """Delete all covers"""
    if rigthVerif.checkAccessRigth("/cover", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        coverGateway.delete()
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/cover/<int:id_cover>', methods=['DELETE'])
@auto.doc('public')
def delete_cover(id_cover):
    """Delete a cover by id"""
    if rigthVerif.checkAccessRigth("/cover/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        coverGateway.delete_id(id_cover)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


###### artist routes ######
@routesBp.route('/artist', methods=['GET'])
@auto.doc('public')
def get_artists():
    """Get all artists"""
    if rigthVerif.checkAccessRigth("/artist", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(artistGateway.get())
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/artist/<int:id_artist>', methods=['GET'])
@auto.doc('public')
def get_artist(id_artist):
    """Get a artist by id"""
    if rigthVerif.checkAccessRigth("/artist/x", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        try:
            return artistGateway.get_id(id_artist).serialize()
        except RuntimeError:
            return "Not found", status.HTTP_404_NOT_FOUND
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/artist', methods=['POST'])
@auto.doc('public')
def create_artist():
    """Create an artist"""
    if rigthVerif.checkAccessRigth("/artist", "POST", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        artist = getAuthor()
        return artistGateway.insert(artist).serialize()
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/artist/<int:id_artist>', methods=['PUT'])
@auto.doc('public')
def update_artist(id_artist):
    """Update an artist by id"""
    if rigthVerif.checkAccessRigth("/artist/x", "PUT", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        artist = getAuthor()
        artist.id_artist = id_artist
        artistGateway.update(artist)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/artist', methods=['DELETE'])
@auto.doc('public')
def delete_artists():
    """Delete all artists"""
    if rigthVerif.checkAccessRigth("/artist", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        artistGateway.delete()
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/artist/<int:id_artist>', methods=['DELETE'])
@auto.doc('public')
def delete_artist(id_artist):
    """Delete an artist by id"""
    if rigthVerif.checkAccessRigth("/artist/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        artistGateway.delete_id(id_artist)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


###### album routes ######
@routesBp.route('/album', methods=['GET'])
@auto.doc('public')
def get_albums():
    """Get all albums"""
    if rigthVerif.checkAccessRigth("/album", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(albumGateway.get())
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/album/<int:id_album>', methods=['GET'])
@auto.doc('public')
def get_album(id_album):
    """Get a album by id"""
    if rigthVerif.checkAccessRigth("/album/x", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        try:
            return albumGateway.get_id(id_album).serialize()
        except RuntimeError:
            return "Not found", status.HTTP_404_NOT_FOUND
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/album', methods=['POST'])
@auto.doc('public')
def create_album():
    """Create an album"""
    if rigthVerif.checkAccessRigth("/album", "POST", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        album = getAlbum()
        return albumGateway.insert(album).serialize()
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/album/<int:id_album>', methods=['PUT'])
@auto.doc('public')
def update_album(id_album):
    """Update an album by id"""
    if rigthVerif.checkAccessRigth("/album/x", "PUT", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        album = getAlbum()
        album.id_album = id_album
        albumGateway.update(album)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/album', methods=['DELETE'])
@auto.doc('public')
def delete_albums():
    """Delete all albums"""
    if rigthVerif.checkAccessRigth("/album", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        albumGateway.delete()
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/album/<int:id_album>', methods=['DELETE'])
@auto.doc('public')
def delete_album(id_album):
    """Delete an album by id"""
    if rigthVerif.checkAccessRigth("/album/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        albumGateway.delete_id(id_album)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


###### music routes ######
@routesBp.route('/music', methods=['GET'])
@auto.doc('public')
def get_musics():
    """Get all musics"""
    if rigthVerif.checkAccessRigth("/music", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(musicGateway.get())
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/music/<int:id_music>', methods=['GET'])
@auto.doc('public')
def get_music(id_music):
    """Get a music by id"""
    if rigthVerif.checkAccessRigth("/music/<int:id_music>", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        try:
            return musicGateway.get_id(id_music).serialize()
        except RuntimeError:
            return "Not found", status.HTTP_404_NOT_FOUND
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/music', methods=['POST'])
@auto.doc('public')
def create_music():
    """recognize a music"""
    # data, samplerate = sf.read(io.BytesIO(response))
    role = authentificator.getRole(request.headers.get('Authorization') or "")
    if rigthVerif.checkAccessRigth("/music", "POST", str(role)):
        requestData = request.get_json()
        audioData:str = requestData['data']
        audioData = audioData.strip("'data:audio/wav;base64,")
        decodedData = base64.b64decode(audioData)
        data = Manager().open_file_from_bytes(decodedData)
        return Manager().identify_sample(data)
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/music/<int:id_music>', methods=['PUT'])
@auto.doc('public')
def update_music(id_music):
    """Update a music by id"""
    if rigthVerif.checkAccessRigth("/music/x", "PUT", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        music = getMusic()
        music.id_music = id_music
        musicGateway.update(music)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/music', methods=['DELETE'])
@auto.doc('public')
def delete_musics():
    """Delete all musics"""
    if rigthVerif.checkAccessRigth("/music", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        albumGateway.delete()
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/music/<int:id_music>', methods=['DELETE'])
@auto.doc('public')
def delete_music(id_music):
    """Delete a music by id"""
    if rigthVerif.checkAccessRigth("/music/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        albumGateway.delete_id(id_music)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN


###### history routes ######
@routesBp.route('/history/<int:iduser>', methods=['GET'])
@auto.doc('public')
def get_history(iduser):
    """Get all history for the user id"""
    if rigthVerif.checkAccessRigth("/history/x", "GET", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        return serializeList(userGateway.get_history(iduser))
    return "Access denied", status.HTTP_403_FORBIDDEN

@routesBp.route('/history/<int:iduser>', methods=['POST'])
@auto.doc('public')
def add_history(iduser):
    """Add a history entry for the user id"""
    if rigthVerif.checkAccessRigth("/history/x", "POST", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        history = getHistory()
        history.id_user = iduser
        return userGateway.insert_history(history).serialize()
    return "Access denied", status.HTTP_403_FORBIDDEN

@routesBp.route('/history/<int:iduser>', methods=['DELETE'])
@auto.doc('public')
def delete_history(iduser):
    """Delete all history for the user id"""
    if rigthVerif.checkAccessRigth("/history/x", "DELETE", str(authentificator.getRole(request.headers.get('Authorization') or ""))):
        userGateway.delete_history(iduser)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN

###### addMusic routes ######
@routesBp.route('/addMusic', methods=['POST'])
@auto.doc('public')
def add_music():
    """add a music"""
    role = authentificator.getRole(request.headers.get('Authorization') or "")
    if rigthVerif.checkAccessRigth("/addMusic", "POST", str(role)):
        requestData = request.get_json()
        audioData:str = requestData['data']
        audioData = audioData.strip("'data:audio/flac;base64,")
        decodedData = base64.b64decode(audioData)
        Manager().add_song(decodedData)
        return "ok",status.HTTP_200_OK
    return "Access denied", status.HTTP_403_FORBIDDEN

###### connexion routes ######
@routesBp.route('/connexion', methods=['DELETE'])
@auto.doc('public')
def connexion():
    """sign out a user"""
    if rigthVerif.checkAccessRigth("/connexion", "DELETE", "user"):
        credential = request.get_json()
        if isinstance(credential, dict) and 'token' in credential.keys() and authentificator.tokenLogin(credential['token']):
            tokenGateway.delete(tokenGateway.get(credential['token']))
            return "ok",status.HTTP_200_OK
        return "User not found", status.HTTP_417_EXPECTATION_FAILED
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/connexion/login', methods=['POST'])
@auto.doc('public')
def login():
    """login a user"""
    if rigthVerif.checkAccessRigth("/connexion/login", "POST", "user"):
        credential: dict | str = request.get_json()
        # if credential are username and password
        if isinstance(credential, dict) and 'username' in credential.keys() and 'password' in credential.keys():
            return authentificator.passwordLogin(credential['username'], credential['password']).serialize()
        if isinstance(credential, dict) and 'token' in credential.keys() and authentificator.tokenLogin(credential['token']):
            return tokenGateway.get(credential['token']).serialize()
        return "User not found", status.HTTP_406_NOT_ACCEPTABLE
    return "Access denied", status.HTTP_403_FORBIDDEN


@routesBp.route('/connexion/signin', methods=['POST'])
@auto.doc('public')
def signin():
    """signin a user"""
    if rigthVerif.checkAccessRigth("/connexion/signin", "POST", "user"):
        credential = request.get_json()
        # if credential are username and password and firstname and lastname
        # credential.firstname and credential.lastname:
        if isinstance(credential, dict) and 'username' in credential.keys() and 'password' in credential.keys() and 'firstname' in credential.keys() and 'lastname' in credential.keys():
            return authentificator.passwordSignin(credential['username'], credential['firstname'], credential['lastname'], credential['password']).serialize()
        return "User not found", status.HTTP_406_NOT_ACCEPTABLE
    return "Access denied", status.HTTP_403_FORBIDDEN

@routesBp.errorhandler(NoMatchException)
def no_match_handler(exception):
    return str(exception), 543

@routesBp.errorhandler(RuntimeError)
def runtime_handler(exception):
    return str(exception), status.HTTP_500_INTERNAL_SERVER_ERROR

def serializeList(liste):
    for i, el in enumerate(liste):
        liste[i] = el.serialize()
    return liste


def getUser() -> User:
    data = request.get_json()
    return User(data['id'], data['userName'], data['firstName'] or "", data['lastName'] or "", Role.fromStr(data['roleName']))


def getAuthor() -> Artist:
    data = request.get_json()
    return Artist(data['id_artist'], data['name'])


def getAlbum() -> Album:
    data = request.get_json()
    return Album(data['id_album'], data['title'], Cover(data['cover']['coverId'], data['cover']['imageBase64']))


def getMusic() -> Music:
    data = request.get_json()
    artist=artistGateway.get_id(data['artist']['id'])
    featuring = []    

    for artist in data['featuring']:
        featuring.append(artistGateway.get_id(artist['id']))

    album = albumGateway.get_id(data['album']['id'])
    return Music(data['id_music'], data['title'], artist, featuring, album)


def getHistory() -> MusicHistory:
    data = request.get_json()
    return MusicHistory(data['id'], data['id_music'], data['id_user'], data['history_date'])


def open_wav(path: str, start: int = 0, end: int = -1) -> AudioData:
    data_reel, fe_reel = sf.read(path, dtype="int16")
    if len(data_reel.shape) == 2:
        x = data_reel.sum(axis=1) / 2
    else:
        x = data_reel
    return AudioData(x[start*fe_reel:-1 if end == -1 else end*fe_reel], fe_reel)
