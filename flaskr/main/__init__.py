from flask import Blueprint

apiversion="v1"
routesBp = Blueprint('main', __name__, url_prefix='/api/'+apiversion)
docBp = Blueprint('doc', __name__, url_prefix='/doc')
routes_testBp = Blueprint('routes_test', __name__, url_prefix='/api/'+apiversion)

#pylint: disable=wrong-import-position
from main import routes, routes_test, doc
from .doc import auto

