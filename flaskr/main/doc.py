"""
Module: main

This module provides functionality for generating documentation endpoints in a Flask application.

Dependencies:
    - flask
    - flask_selfdoc

Classes:
    None

Functions:
    public_doc():
        Returns HTML documentation for the public API endpoints.
    
    private_doc():
        Returns HTML documentation for the private API endpoints.
"""

from flask import Blueprint
from flask_selfdoc import Autodoc

auto = Autodoc()
doc_bp = Blueprint('doc', __name__)

@doc_bp.route('/')
@doc_bp.route('/public')
def public_doc():
    """
    Returns HTML documentation for the public API endpoints.

    Parameters:
        None

    Returns:
        str: HTML documentation for the public API endpoints.
    """
    return auto.html(groups=['public'], title='Public Documentation')

@doc_bp.route('/private')
def private_doc():
    """
    Returns HTML documentation for the private API endpoints.

    Parameters:
        None

    Returns:
        str: HTML documentation for the private API endpoints.
    """
    return auto.html(groups=['private'], title='Private Documentation')