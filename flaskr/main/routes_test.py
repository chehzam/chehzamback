"""
Module: audio_routes

This module defines the API routes for audio-related operations.

Dependencies:
    - soundfile
    - main.routesBp
    - dataGeneration.data_generator.DataGenerator
    - model.audio_data.AudioData
    - dataModifiers.audio_modifier.AudioModifier
    - gestion.manager.manager.Manager
    - flask.render_template

Functions:
    open_wav(path: str, start: int = 0, end: int = -1) -> AudioData:
        Opens a WAV file and returns the audio data within the specified time range.

API Routes:
    test_spectrogram() -> str:
        Generates spectrogram images for test audio files and renders the spectrogram.html template.

    peak_extractor() -> str:
        Performs peak extraction on a test audio file and renders the peaks.html template.

    peak_spectrum() -> str:
        Generates a plot of the spectrum with highlighted peaks for a test audio file and renders the peaks_spectrum.html template.

    hash_generator() -> dict[str, int]:
        Generates a hash for a test audio file and returns the hash dictionary.

    hash_match() -> str:
        Identifies a sample audio file using the Manager class and returns the identification result.

    import_music() -> str:
        Imports music from a directory using the Manager class.
"""

import soundfile as sf
from main import routesBp
from dataGeneration.data_generator import DataGenerator
from model.audio_data import AudioData
from dataModifiers.audio_modifier import AudioModifier
from gestion.manager.manager import Manager
from flask import render_template

########################
###### API routes ######
########################

def open_wav(path: str, start: int = 0, end: int = -1) -> AudioData:
    """Opens a WAV file and returns the audio data within the specified time range.

        Parameters:
            path (str): The path to the WAV file.
            start (int, optional): The start time (in seconds) of the audio data. Defaults to 0.
            end (int, optional): The end time (in seconds) of the audio data. Defaults to -1, which indicates the end of the file.

        Returns:
            AudioData: The audio data within the specified time range.
    """
    data_reel, fe_reel = sf.read(path, dtype="int16")
    if len(data_reel.shape) == 2:
        x = data_reel.sum(axis=1) / 2
    else:
         x = data_reel
    return AudioData(x[start*fe_reel:-1 if end == -1 else end*fe_reel], fe_reel)

@routesBp.route('/spectrogram', methods=['GET'])
def test_spectrogram():
    """Generates spectrogram images for test audio files and renders the spectrogram.html template.

        Parameters:
            None

        Returns:
            str: The rendered HTML template for the spectrogram page.
    """
    data_generator = DataGenerator()
    audio_modifier = AudioModifier()

    data = open_wav("tests/freeze2.wav",0, 10)
    audio_modifier.audio_data = data
    data_generator.audio_data = audio_modifier.normalize()
    data_generator.export_spectrogram("test1", "static/img")

    data = open_wav("tests/freeze.wav", 56, 66)
    audio_modifier.audio_data = data
    data_generator.audio_data = audio_modifier.normalize()
    data_generator.export_spectrogram("test2", "static/img")

    return render_template('spectrogram.html')

@routesBp.route('/peak_extractor', methods=['GET'])
def peak_extractor():
    """Performs peak extraction on a test audio file and renders the peaks.html template.

        Parameters:
            None

        Returns:
            str: The rendered HTML template for the peak extraction page.
    """
    data_generator = DataGenerator()
    audio_modifier = AudioModifier()

    data = open_wav("tests/freeze2.wav")
    audio_modifier.audio_data = data
    data = audio_modifier.normalize()
    data_generator.audio_data = data
    data_generator.export_peaks("test", "static/img")
    return render_template('peaks.html')

@routesBp.route('/peak_spectrum', methods=['GET'])
def peak_spectrum():
    """Generates a plot of the spectrum with highlighted peaks for a test audio file and renders the peaks_spectrum.html template.

        Parameters:
            None

        Returns:
            str: The rendered HTML template for the peak spectrum page.
    """
    data_generator = DataGenerator()
    audio_modifier = AudioModifier()

    data = open_wav("tests/freeze.wav")
    audio_modifier.audio_data = data
    data_generator.audio_data = audio_modifier.normalize()
    data_generator.export_peaks_spectrum("test", "static/img")
    return render_template('peaks_spectrum.html')

@routesBp.route('/hash_generator', methods=['GET'])
def hash_generator():
    """Generates a hash for a test audio file and returns the hash dictionary.

        Parameters:
            None

        Returns:
            dict[str, int]: The generated hash dictionary.
    """
    data_generator = DataGenerator()
    audio_modifier = AudioModifier()

    data = open_wav("tests/freeze.wav")
    audio_modifier.audio_data = data
    data_generator.audio_data = audio_modifier.normalize()
    hashing = data_generator.get_hash()

    return hashing

@routesBp.route('/hash_match', methods=['GET'])
def hash_match():
    """Identifies a sample audio file using the Manager class and returns the identification result.

        Parameters:
            None

        Returns:
            str: The identification result.
    """
    data = open_wav("tests/test.flac",start=1,end=5)
    return Manager().identify_sample(data)

@routesBp.route('/import_music', methods=['GET'])
def import_music():
    """Imports music from a directory using the Manager class.

        Parameters:
            None

        Returns:
            str: A status message indicating the success of the import operation.
    """
    manager = Manager()
    manager.import_songs("tests/import")

    return "200"