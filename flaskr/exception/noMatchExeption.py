"""
Module: nomatchexception

This module defines an exception class for representing a "No Match" scenario in a database.

Classes:
    NoMatchException:
        An exception class representing a "No Match" scenario in a database.

Inherited From:
    Exception
"""

class NoMatchException(Exception):
    """
    Exception class representing a "No Match" scenario in a database.

    Inherits From:
        Exception

    Attributes:
        message (str): The error message describing the "No Match" scenario.

    Methods:
        __init__(self, message="No match found in database"):
            Initializes a new instance of the NoMatchException class.
    """

    def __init__(self, message="No match found in database"):
        """
        Initializes a new instance of the NoMatchException class.

        Parameters:
            message (str, optional): The error message describing the "No Match" scenario.
                Defaults to "No match found in database".
        """
        self.message = message
        super().__init__(self.message)