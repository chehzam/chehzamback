from __future__ import annotations
import numpy as np
from scipy import signal
from model.audio_data import AudioData

class FrequencyFilter():
    """A class used to apply frequency filters to audio data.

    Attributes:
    ----------
    audio_data : AudioData | None
        Audio data to be filtered.

    Methods:
    -------
    apply_lowpass(passband_edge: float = 3500, stopband_edge: float = 4000) -> AudioData:
        Apply a low-pass filter to the audio data.

    """

    def __init__(self) -> None:
        self.audio_data: AudioData | None = None

    def apply_lowpass(self, passband_edge: float = 3500, stopband_edge: float = 4000) -> AudioData:
        """Apply a low-pass filter to the audio data.

        Parameters:
        ----------
        passband_edge : float, optional
            The frequency at which the filter transition band begins (in Hz). Default is 3500 Hz.
        stopband_edge : float, optional
            The frequency at which the filter stops attenuating (in Hz). Default is 4000 Hz.

        Returns:
        -------
        AudioData
            The filtered audio data as an AudioData object.

        Raises:
        ------
        RuntimeError
            If self.audio_data is not instantiated.
        """
        if self.audio_data is not None:
            f = self.audio_data.f_sample
            if f >= stopband_edge:
                n, wn = signal.buttord(wp=passband_edge, ws=stopband_edge, gpass=1, gstop=40, fs=f)
                b, a = signal.butter(n, wn, 'low', fs=f)
                x = signal.lfilter(b, a, self.audio_data.data.copy())
                return AudioData(np.array(x), f, True)
            return self.audio_data
        raise RuntimeError("self.audio_data must be instanciated.")
