from __future__ import annotations
from math import gcd
from scipy import signal
from model.audio_data import AudioData

class Downsampler():
    """
    A class for downsampling audio data.

    Parameters
    ----------
    None

    Attributes
    ----------
    audio_data : AudioData | None
        The audio data to downsample.

    Methods
    -------
    apply_downsampling(target_fs: float) -> AudioData:
        Apply downsampling to the audio data.
    
    """

    def __init__(self) -> None:
        self.audio_data: AudioData | None = None

    def apply_downsampling(self, target_fs: float) -> AudioData:
        """
        Applies downsampling to the audio data.

        Parameters
        ----------
        target_fs : float
            The target sampling frequency.

        Returns
        -------
        AudioData
            The downsampled audio data.

        Raises
        ------
        RuntimeError
            If self.audio_data is not instanciated.
        """
        data = self.audio_data
        if data:
            f_src = data.f_sample
            filter_freq = target_fs//gcd(int(target_fs), int(f_src)) * f_src
            numtaps, beta = signal.kaiserord(65, 24/(0.5*target_fs))
            hn = signal.firwin(numtaps, target_fs/filter_freq*2, window=('kaiser', beta)) # type: ignore
            downsample = signal.resample_poly(data.data, target_fs, f_src, window=hn)
            return AudioData(downsample, target_fs, True)
        raise RuntimeError("self.__audio_data must be instanciated.")
