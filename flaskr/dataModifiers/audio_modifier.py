from __future__ import annotations
from model.audio_data import AudioData
from .frequency_filter import FrequencyFilter
from .downsampler import Downsampler

class AudioModifier():
    """A class for modifying audio data.

    Attributes:
        _audio_data (AudioData | None): The audio data to be modified.
        frequency_filter (FrequencyFilter): The frequency filter object to use for filtering the audio data.
        downsampler (Downsampler): The downsampler object to use for downsampling the audio data.

    Methods:
        audio_data (property): The property getter for the audio data.
        audio_data (setter): The property setter for the audio data.
        normalize(): Downsamples the audio data to 8000 Hz.

    """

    def __init__(self) -> None:
        self._audio_data: AudioData | None = None
        self.frequency_filter: FrequencyFilter = FrequencyFilter()
        self.downsampler: Downsampler = Downsampler()
    
    @property
    def audio_data(self) -> AudioData | None:
        """Getter for the audio data.

        Returns:
            AudioData | None: The audio data being modified.
        """
        return self._audio_data
    
    @audio_data.setter
    def audio_data(self, audio_data: AudioData):
        """Setter for the audio data.

        Args:
            audio_data (AudioData): The audio data to modify.
        """
        self.frequency_filter.audio_data = audio_data
        self.downsampler.audio_data = audio_data
        self._audio_data = audio_data

    def normalize(self) -> AudioData:
        """Downsamples the audio data to 8000 Hz.

        Returns:
            AudioData: The downsampled audio data.
        """
        try:
            return self.downsampler.apply_downsampling(8000)
        except RuntimeError as e:
            raise e