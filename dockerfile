# pull official base image
FROM python:3.10.11-buster
# set work directory
WORKDIR /usr/src/app
# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt
# copy project
COPY . /usr/src/app/
EXPOSE 5000

ENV DATABASE_HOST=chehzambdd2.postgres.database.azure.com

# run server
CMD ["/bin/bash", "-c", "cd ./flaskr && python -m flask run --host=0.0.0.0 --port=80"]