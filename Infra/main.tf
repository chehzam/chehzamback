terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.73.0"
    }

    random = {
      source = "hashicorp/random"
      version = "3.5.1"
    }
  }

  # backend "http" {}
}

provider "random" {}

provider "azurerm" {
  skip_provider_registration = true
  features {}

  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  subscription_id = var.subscription_id
}

variable "client_id" {
  type = string
}

variable "client_secret" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "subscription_id" {
  type = string
}

resource "random_integer" "ri" {
  min = 10000
  max = 99999
}

data "azurerm_resource_group" "chehzam" {
  name     = "chehzam"
}

resource "azurerm_postgresql_flexible_server" "bdd" {
  name                   = "chehzambdd2"
  resource_group_name    = data.azurerm_resource_group.chehzam.name
  location               = data.azurerm_resource_group.chehzam.location
  administrator_login    = "postgres"
  administrator_password = "admin"
  version                = "12"
  zone                   = "1"



  storage_mb = 32768

  sku_name   = "GP_Standard_D4s_v3"
}

resource "azurerm_postgresql_flexible_server_database" "CHEHZAM" {
  name      = "CHEHZAM"
  server_id = azurerm_postgresql_flexible_server.bdd.id
  collation = "fr_FR.utf8"
  charset   = "utf8"

  # prevent the possibility of accidental data loss
  lifecycle {
    prevent_destroy = true
  }
}


resource "azurerm_postgresql_flexible_server_firewall_rule" "toutIp" {
  name                = "toutIp"
  server_id = azurerm_postgresql_flexible_server.bdd.id
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}


resource "azurerm_service_plan" "sp" {
  name                = "sp_Back"
  resource_group_name = data.azurerm_resource_group.chehzam.name
  location            = data.azurerm_resource_group.chehzam.location
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "webApp" {
  name                = "chehzamwebAppBack"
  resource_group_name = data.azurerm_resource_group.chehzam.name
  location            = azurerm_service_plan.sp.location
  service_plan_id     = azurerm_service_plan.sp.id

  site_config {
    application_stack {
      docker_image_name = "coucou619/chehzam:backend"
      docker_registry_url = "https://index.docker.io"
      docker_registry_username = "coucou619"
      docker_registry_password = "coucoucou"
    }
  }

  app_settings = {
    SCM_DO_BUILD_DURING_DEPLOYMENT = "true"
  }
}